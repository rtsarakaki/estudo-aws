import express from 'express';
const router = express.Router();

import * as tenantController from '../controllers/tenant.controller';
router.get('/tenant', tenantController.list);
router.get('/tenant/:sortedKey', tenantController.getById);
router.get('/tenant/bycontext/:context', tenantController.getByContext);
router.get('/tenant/byname/:name', tenantController.getByName);
router.post('/tenant', tenantController.create);
router.patch('/tenant', tenantController.update);
router.delete('/tenant/:sortedKey', tenantController.remove);

const menuCardController = require('../controllers/card-menu.controller')
router.get('/card-menu', menuCardController.list);
router.get('/card-menu/:sortedKey', menuCardController.getById);
router.post('/card-menu', menuCardController.create);
router.patch('/card-menu', menuCardController.update);
router.delete('/card-menu/:sortedKey', menuCardController.remove);
router.get('/card-menu/bycontext/:context', menuCardController.getByContext);
router.get('/card-menu/byname/:name', menuCardController.getByName);

module.exports = router;