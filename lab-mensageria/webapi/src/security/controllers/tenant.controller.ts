import express from "express";

import * as dotenv from "dotenv";
dotenv.config();

import * as dynamodbService from "../../shared/dynamodb/dynamodb.service";

const PREFIX = "tenant";

export async function create(req: any, res: any, next: any) {
  req.body.sort = req.body.name;
  req.body.context = PREFIX;
  const result = await dynamodbService.create(
    req.body,
    PREFIX,
    <string>process.env.SECURITY_TABLENAME
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function update(req: any, res: any, next: any) {
  req.body.context = PREFIX;  
  const result = await dynamodbService.update(
    req.body,
    <string>process.env.SECURITY_TABLENAME
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function remove(req: any, res: any, next: any) {
  const result = await dynamodbService.remove(
    PREFIX,
    req.params.sortedKey,
    <string>process.env.SECURITY_TABLENAME
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function list(req: any, res: any, next: any) {
  const result = await dynamodbService.list(
    PREFIX,
    <string>process.env.SECURITY_TABLENAME
  );
  try {
    console.log(result);
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function getById(req: any, res: any, next: any) {
  const result = await dynamodbService.getById(
    PREFIX,
    req.params.sortedKey,
    <string>process.env.SECURITY_TABLENAME
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function getByName(req: any, res: any, next: any) {
  const result = await dynamodbService.getByName(
    PREFIX,
    req.params.name,
    <string>process.env.SECURITY_TABLENAME,
    req.params.limit
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}

export async function getByContext(req: any, res: any, next: any) {
  const result = await dynamodbService.getByContext(
    PREFIX,
    req.params.context,
    <string>process.env.SECURITY_TABLENAME,
    req.params.limit
  );
  try {
    res.status(200).send(result);
  } catch (e) {
    res.status(404).send(e);
  }
}
