import {
  DeleteItemCommand,
  DeleteItemCommandInput,
  DynamoDBClient,
  GetItemCommand,
  GetItemCommandInput,
  PutItemCommand,
  PutItemCommandInput,
  QueryCommand,
  QueryCommandInput,
} from "@aws-sdk/client-dynamodb";
import { marshall, unmarshall } from "@aws-sdk/util-dynamodb";
import AWS from "aws-sdk";
import * as dotenv from "dotenv";
import moment from "moment";

//const { unmarshall } = require("@aws-sdk/util-dynamodb");

import { DynamoDbItem, DynamoDbResult } from "./dynamodb-item.interface";

/**
 * Required External Modules
 */
dotenv.config();

const config = {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
};

const client = new DynamoDBClient(config);

const docClient = new AWS.DynamoDB.DocumentClient(config);

/**
 * Data Model Interfaces
 */

/**
 * CRUD Methods
 */

export const create = async (
  item: DynamoDbItem,
  prefix: string,
  tableName: string
): Promise<any> => {
  const date = new Date();
  let unique_id = moment().unix(); // ou uuid()

  item.partitionKey = prefix;
  item.sortedKey = prefix + "|" + unique_id;
  item.searchName = cleanText(item.name);
  item.searchContext = prefix;
  item.created = date.toLocaleDateString() + " " + date.toLocaleTimeString();

  const params: PutItemCommandInput = {
    TableName: tableName,
    Item: marshall(item),
  };

  let res = new DynamoDbResult();

  try {
    const result = await client.send(new PutItemCommand(params));
    res = {
      status: result.$metadata.httpStatusCode,
      message: "Success",
      method: "create",
      IsSuccess: true,
      data: item,
      //result: result,
    };
    return res;
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "create",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

export const update = async (
  item: DynamoDbItem,
  tableName: string
): Promise<any> => {
  const date = new Date();

  item.modified = date.toLocaleDateString() + " " + date.toLocaleTimeString();
  item.searchName = cleanText(item.name);
  item.searchContext = cleanText(item.context);

  const params: PutItemCommandInput = {
    TableName: tableName,
    Item: marshall(item),
    ConditionExpression:
      "partitionKey = :partitionKey AND sortedKey = :sortedKey",
    ExpressionAttributeValues: marshall({
      ":partitionKey": item.partitionKey,
      ":sortedKey": item.sortedKey,
    }),
  };

  let res = new DynamoDbResult();

  try {
    console.log(params);
    const result = await client.send(new PutItemCommand(params));
    if (result.$metadata.httpStatusCode === 200) {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "update",
        IsSuccess: true,
        data: item,
        //result: result,
      };
      console.log(res);
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "update",
        IsSuccess: false,
        result: result,
      };
      console.log(res);
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "update",
      IsSuccess: false,
      error: err,
    };
    console.log(res);
    return res;
  }
};

export const remove = async (
  partitionKey: string,
  sortedKey: string,
  tableName: string
): Promise<any> => {
  const params: DeleteItemCommandInput = {
    TableName: tableName,
    Key: marshall({
      partitionKey: partitionKey,
      sortedKey: sortedKey,
    }),
    ConditionExpression:
      "partitionKey = :partitionKey AND sortedKey = :sortedKey",
    ExpressionAttributeValues: marshall({
      ":partitionKey": partitionKey,
      ":sortedKey": sortedKey,
    }),
  };

  let res = new DynamoDbResult();

  try {
    const result = await client.send(new DeleteItemCommand(params));
    if (result.$metadata.httpStatusCode === 200) {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "remove",
        IsSuccess: true,
        //result: result,
      };
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "remove",
        IsSuccess: false,
        result: result,
      };
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "remove",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

export const list = async (
  partitionKey: string,
  tableName: string,
  limit?: number,
  startKey?: string
): Promise<any> => {
  limit = limit ? limit : 20;
  const params: QueryCommandInput = {
    TableName: tableName,
    KeyConditionExpression: "#partitionKey = :partitionKey",
    ExpressionAttributeNames: {
      "#partitionKey": "partitionKey",
    },
    ExpressionAttributeValues: marshall({
      ":partitionKey": partitionKey,
    }),
    Limit: limit,
  };

  if (startKey) {
    params.ExclusiveStartKey = marshall({
      partitionKey: partitionKey,
      sortedKey: startKey
    });
  }

  var parse = AWS.DynamoDB.Converter.output;
  let res = new DynamoDbResult();

  try {
    const result = await client.send(new QueryCommand(params));
    if (result.Items) {

      const items = result.Items.map(
        (item) => unmarshall(item)
      );

      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "list",
        IsSuccess: true,
        //result: result,
        data: items,
      };
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "list",
        IsSuccess: false,
        error: result,
      };
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "list",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

export const getById = async (
  partitionKey: string,
  sortedKey: string,
  tableName: string
): Promise<any> => {
  const params: GetItemCommandInput = {
    TableName: tableName,
    Key: marshall({
      partitionKey: partitionKey,
      sortedKey: sortedKey,
    }),
  };

  let res = new DynamoDbResult();

  try {
    const result = await client.send(new GetItemCommand(params));
    if (result.Item) {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "getById",
        IsSuccess: true,
        //result: result,
        data: unmarshall(result.Item),
      };
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "getById",
        IsSuccess: false,
        result: result,
      };
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "getById",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

export const getByContext = async (
  partitionKey: string,
  context: string,
  tableName: string,
  limit?: number
): Promise<any> => {
  limit = limit ? limit : 20;
  const params: QueryCommandInput = {
    TableName: tableName,
    IndexName: "context-lsi",
    KeyConditionExpression:
      "#partitionKey = :partitionKey and #context = :context",
    ExpressionAttributeNames: {
      "#partitionKey": "partitionKey",
      "#context": "searchContext",
    },
    ExpressionAttributeValues: marshall({
      ":partitionKey": partitionKey,
      ":context": cleanText(context),
    }),
    Limit: limit,
  };

  let res = new DynamoDbResult();

  try {
    const result = await client.send(new QueryCommand(params));
    if (result.Items) {
      const items = result.Items.map(
        (item) => unmarshall(item)
      );

      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "getByContext",
        IsSuccess: true,
        //result: result,
        data: items,
      };
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "getByContext",
        IsSuccess: false,
        error: result,
      };
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "getByContext",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

export const getByName = async (
  partitionKey: string,
  name: string,
  tableName: string,
  limit?: number
): Promise<any> => {
  limit = limit ? limit : 20;
  const params: QueryCommandInput = {
    TableName: tableName,
    IndexName: "name-lsi",
    KeyConditionExpression:
      "#partitionKey = :partitionKey and begins_with(#name, :name)",
    ExpressionAttributeNames: {
      "#partitionKey": "partitionKey",
      "#name": "searchName",
    },
    ExpressionAttributeValues: marshall({
      ":partitionKey": partitionKey,
      ":name": cleanText(name),
    }),
    Limit: limit,
  };

  let res = new DynamoDbResult();

  try {
    const result = await client.send(new QueryCommand(params));
    if (result.Items) {

      const items = result.Items.map(
        (item) => unmarshall(item)
      );

      res = {
        status: result.$metadata.httpStatusCode,
        message: "Success",
        method: "getByName",
        IsSuccess: true,
        //result: result,
        data: items,
      };
      return res;
    } else {
      res = {
        status: result.$metadata.httpStatusCode,
        message: "Fail",
        method: "getByName",
        IsSuccess: false,
        error: result,
      };
      return res;
    }
  } catch (err) {
    res = {
      status: 404,
      message: "Error",
      method: "getByName",
      IsSuccess: false,
      error: err,
    };
    return res;
  }
};

/**
 * Util functions
 */

function cleanText(text: string) {
  if (text) {
    text = text.toLowerCase();
    text = text.replace(new RegExp("[ÁÀÂÃ]", "gi"), "a");
    text = text.replace(new RegExp("[ÉÈÊ]", "gi"), "e");
    text = text.replace(new RegExp("[ÍÌÎ]", "gi"), "i");
    text = text.replace(new RegExp("[ÓÒÔÕ]", "gi"), "o");
    text = text.replace(new RegExp("[ÚÙÛ]", "gi"), "u");
    text = text.replace(new RegExp("[Ç]", "gi"), "c");
    return text;
  } else {
    return "";
  }
}
