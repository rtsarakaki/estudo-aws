export interface DynamoDbItem {
  partitionKey: string;
  sortedKey: string;
  name: string;
  context: string;
  searchName: string;
  searchContext: string;
  sort: string;
  created: string;
  modified: string;
}

export class DynamoDbResult {
  status!: number | undefined;
  message!: string;
  method?: string;
  IsSuccess!: boolean;
  error?: any;
  result?: any;
  data?: any;
}
