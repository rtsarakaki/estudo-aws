import * as dotenv from "dotenv";
dotenv.config();

import AWS from "aws-sdk";
AWS.config.update({ region: 'us-east-1' });

const config = {
  endpoint: new AWS.Endpoint(<string>process.env.AWS_SQS_ENDPOINT),
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
};

const sqs = new AWS.SQS(config);

exports.receiveMessage = async (req:any, res:any, next:any) => {

  try {
    const params = req.body;
    console.log(params);

    sqs.receiveMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Sucesso.',
          result: data
        }

        return res.status(200).send(response);

      }
    });

  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.sendMessage = async (req:any, res:any, next:any) => {

  try {
    const params = req.body;
    console.log(params);

    sqs.sendMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Mensagem criada com sucesso.',
          result: data
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.deleteMessage = async (req:any, res:any, next:any) => {
  try {

    const params = req.body;

    // console.log(req.body);
    console.log(params);

    sqs.deleteMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Mensagem apagada com sucesso.'
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.changeMessageVisibility = async (req:any, res:any, next:any) => {
  try {

    const params = req.body;

    // console.log(req.body);
    console.log(params);

    sqs.changeMessageVisibility(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'VisibilityTimeout alterado com sucesso.'
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};