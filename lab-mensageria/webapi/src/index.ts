/**
 * Required External Modules
 */
import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import helmet from "helmet";

dotenv.config();

/**
 * App Variables
 */
if (!process.env.PORT) {
  process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const app = express();

/**
 *  App Configuration
 */
app.use(helmet());
app.use(cors());
app.use(express.json());

/**
 * Server Activation
 */
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

//import * as securityRoutes from '../src/security/routes/security.routes';
//const tenantController = require('../src/security/controllers/tenant.controllers');
const awsSqsRoutes = require('./aws/routes/aws-sqs.routes')
app.use('/aws-sqs', awsSqsRoutes);
const securityRoutes = require('./security/routes/security.routes')
app.use('/security', securityRoutes);