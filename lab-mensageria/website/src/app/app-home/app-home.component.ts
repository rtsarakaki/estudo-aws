import { AppGlobal } from './../app.global';
import { CardMenuService } from './../security/services/card-menu.service';
import { Component, OnInit } from '@angular/core';
import { CardMenu } from '../security/models/cardMenu';

@Component({
  selector: 'app-app-home',
  templateUrl: './app-home.component.html',
  styleUrls: ['./app-home.component.scss']
})
export class AppHomeComponent implements OnInit {
  cards! : any;

  constructor(private cardMenuService: CardMenuService) { }

  ngOnInit(): void {
    this.getCardList();
  }

  getCardList() {
    this.cardMenuService.list().subscribe(
      (success: any) => {
        this.cards = success.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  get tenantURL() {
    return AppGlobal.tenantURL;
  }
}
