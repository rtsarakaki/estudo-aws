import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HelpHttprequestComponent } from './doc/help-httprequest/help-httprequest.component';
import { HelpNovoprojetoangularComponent } from './doc/help-novoprojetoangular/help-novoprojetoangular.component';
import { HelpNovoprojetonodeComponent } from './doc/help-novoprojetonode/help-novoprojetonode.component';
import { IndiceHelpComponent } from './doc/indice-help/indice-help.component';
import { SecurityModule } from './security/security.module';
import { SharedModule } from './shared/shared.module';
import { SqsModule } from './sqs/sqs.module';
import { AppHomeComponent } from './app-home/app-home.component';
import { AppPageNotFoundComponent } from './app-page-not-found/app-page-not-found.component';
import { AppPublicComponent } from './app-public/app-public.component';

@NgModule({
  declarations: [
    AppComponent,
    IndiceHelpComponent,
    HelpNovoprojetoangularComponent,
    HelpHttprequestComponent,
    HelpNovoprojetonodeComponent,
    AppHomeComponent,
    AppPageNotFoundComponent,
    AppPublicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    SqsModule,
    SecurityModule,
    AuthModule,
    ModalModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
