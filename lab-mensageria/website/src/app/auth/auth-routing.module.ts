import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConfirmRegistrationComponent } from './views/confirm/confirmRegistration.component';
import { ForgotPasswordStep1Component } from './views/forgot/forgotPasswordStep1.component';
import { ForgotPasswordStep2Component } from './views/forgot/forgotPasswordStep2.component';
import { JwtComponent } from './views/jwttokens/jwt.component';
import { NewPasswordComponent } from './views/newpassword/newpassword.component';
import { MyProfileComponent } from './views/profile/myprofile.component';
import { ResendCodeComponent } from './views/resend/resendCode.component';
import { SignInComponent } from './views/sign-in/sign-in.component';
import { SignUpComponent } from './views/sign-up/sign-up.component';

const routes: Routes = [
  { path: 'signin', component: SignInComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'forgot', component: ForgotPasswordStep1Component },
  { path: 'forgotPassword/:email', component: ForgotPasswordStep2Component },
  { path: 'resendCode', component: ResendCodeComponent },
  { path: 'confirmRegistration/:username', component: ConfirmRegistrationComponent },
  { path: 'newPassword', component: NewPasswordComponent },
  { path: 'jwt', component: JwtComponent },
  { path: 'profile', component: MyProfileComponent },
  { path: '', redirectTo: '/auth/signin', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
