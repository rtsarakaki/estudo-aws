import { Tenant } from './../security/models/tenant';
import { TenantService } from './../security/services/tenant.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';
import { AppGlobal } from '../app.global';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private tenantService: TenantService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (route.params['tenant']) {
      AppGlobal.tenantURL = route.params['tenant'];
      let isAuth = this.authService.isLoggedIn();
      if (!isAuth) {
        this.router.navigate([route.params['tenant'] + '/auth/signin']);
      }
      return isAuth;
    } else {
      this.router.navigate([route.params['tenant']]);
      return false;
    }
  }
}
