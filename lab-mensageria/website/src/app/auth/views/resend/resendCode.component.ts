import { AppGlobal } from './../../../app.global';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
} from '../../services/auth.service';
import { AlertTypes } from './../../../shared/controls/alert/modal.service';
import { HeaderFrameComponent } from './../control/header-frame/header-frame.component';

@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './resendCode.html',
  styleUrls: ['../css/auth.scss'],
})
export class ResendCodeComponent implements CognitoCallback {
  form!: FormGroup;
  submitted = false;
  @ViewChild('frame') frame!: HeaderFrameComponent;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
    });
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    throw new Error('Method not implemented.');
  }

  resendCode() {
    this.submitted = true;
    if (this.form.valid) {
      this.authService.resendCode(this.form.get('email')?.value, this);
    }
  }

  cognitoCallback(error: any, result: any) {
    if (error != null) {
      this.frame.showAlert('Por favor, tente novamente!', AlertTypes.INFO);
    } else {
      this.router.navigate([
        this.tenantURL + '/auth/confirmRegistration',
        this.form.get('email')?.value,
      ]);
    }
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
