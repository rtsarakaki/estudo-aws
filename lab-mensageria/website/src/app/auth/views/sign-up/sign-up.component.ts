import { AppGlobal } from './../../../app.global';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';
import { environment } from 'src/environments/environment';

import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
  RegistrationUser,
} from '../../services/auth.service';
import { HeaderFrameComponent } from './../control/header-frame/header-frame.component';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['../css/auth.scss'],
})
export class SignUpComponent implements CognitoCallback, OnInit {
  form!: FormGroup;
  submitted = false;
  registrationUser = new RegistrationUser();

  @ViewChild('frame') frame!: HeaderFrameComponent;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ],
      ],
      email: [null, [Validators.required, Validators.email]],
      phoneNumber: [null, [Validators.required]],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(16),
          Validators.pattern(environment.REGEX_PASSWORD),
        ],
      ],
    });
  }

  onRegister() {
    this.submitted = true;
    if (this.form.valid) {
      this.registrationUser.email = this.form.get('email')?.value;
      this.registrationUser.name = this.form.get('name')?.value;
      this.registrationUser.phoneNumber = this.form.get('phoneNumber')?.value;
      this.registrationUser.password = this.form.get('password')?.value;

      this.authService.register(this.registrationUser, this);
    }
  }

  cognitoCallback(message: any, result: any): void {
    if (message != null) {
      //error
      // console.log('result: ' + message);
      this.frame.showAlert(message, AlertTypes.DANGER);
    } else {
      //success -> move to the next step
      console.log(result);
      this.router.navigate([this.tenantURL + '/auth/confirmRegistration', result.user.username]);
    }
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {}

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
