import { AppGlobal } from './../../../app.global';
import { AlertTypes } from './../../../shared/controls/alert/modal.service';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import {
  AuthService,
  Callback,
  LoggedInCallback,
  RegistrationUser,
} from '../../services/auth.service';
import { UserParametersService } from '../../services/user-parameters.service';

@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './myprofile.html',
})
export class MyProfileComponent implements LoggedInCallback {
  public parameters: Array<Parameters> = [];
  public cognitoId!: String;
  @ViewChild('alert') alert!: AlertDismissibleComponent;

  constructor(
    private router: Router,
    private userParams: UserParametersService,
    private authService: AuthService
  ) {
    this.authService.isAuthenticated(this);
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      this.router.navigate([this.tenantURL + '/auth/signin']);
    } else {
      this.userParams.getParameters(
        new GetParametersCallback(this, this.authService)
      );
    }
  }

  onChangeUser(name: string) {
    let alertMsg = this.alert;
    let user = new RegistrationUser();
    user.name = name;//'_' + this.parameters[2].value;
    this.authService.update(user, function (err: any, result: any) {
      if (err) {
        alertMsg.show('Oops, algo deu errado.', err, AlertTypes.DANGER);
      }
      else {
        alertMsg.show('Sucesso!', 'Nome alterado com sucesso!', AlertTypes.SUCCESS);
      }
    });
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}

export class Parameters {
  name!: string;
  value!: string;
}

export class GetParametersCallback implements Callback {
  constructor(
    private me: MyProfileComponent,
    private authService: AuthService
  ) {}

  callback() {}

  callbackWithParam(result: any) {
    for (let i = 0; i < result.length; i++) {
      let parameter = new Parameters();
      parameter.name = result[i].getName();
      parameter.value = result[i].getValue();
      this.me.parameters.push(parameter);
    }

    let param = new Parameters();
    param.name = 'cognito ID';
    param.value = this.authService.getCognitoIdentity();
    this.me.parameters.push(param);
  }
}
