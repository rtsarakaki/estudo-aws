import { AlertTypes } from './../../../../shared/controls/alert/modal.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';

@Component({
  selector: 'ctrl-header-frame',
  templateUrl: './header-frame.component.html',
  styleUrls: ['../../css/auth.scss'],
})
export class HeaderFrameComponent implements OnInit {
  @Input() title!: string;
  @ViewChild('alert') alert!: AlertDismissibleComponent;

  constructor() { }

  ngOnInit(): void {
  }

  showAlert(message: string, type: AlertTypes) {
    this.alert.show('remover', message, type);
  }

}
