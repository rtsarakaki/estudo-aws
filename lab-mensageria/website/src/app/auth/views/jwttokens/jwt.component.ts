import {Component} from "@angular/core";
import {Router} from "@angular/router";
import { AuthService, Callback, LoggedInCallback } from "../../services/auth.service";


export class Stuff {
    public accessToken!: string;
    public idToken!: string;
}

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './jwt.html'
})
export class JwtComponent implements LoggedInCallback {

    public stuff: Stuff = new Stuff();

    constructor(public router: Router, public authService: AuthService) {
        this.authService.isAuthenticated(this);
        console.log("in JwtComponent");

    }

    isLoggedIn(message: string, isLoggedIn: boolean) {
        if (!isLoggedIn) {
            this.router.navigate(['/home/login']);
        } else {
            this.authService.getAccessToken(new AccessTokenCallback(this));
            this.authService.getIdToken(new IdTokenCallback(this));
        }
    }
}

export class AccessTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result: any) {
        this.jwt.stuff.accessToken = result;
    }
}

export class IdTokenCallback implements Callback {
    constructor(public jwt: JwtComponent) {

    }

    callback() {

    }

    callbackWithParam(result: any) {
        this.jwt.stuff.idToken = result;
    }
}
