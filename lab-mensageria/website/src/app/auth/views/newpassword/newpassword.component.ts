import { AppGlobal } from './../../../app.global';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';
import { environment } from 'src/environments/environment';

import { AuthService, ChallengeParameters, CognitoCallback } from '../../services/auth.service';
import { HeaderFrameComponent } from './../control/header-frame/header-frame.component';

export class NewPasswordUser {
  username!: string;
  existingPassword!: string;
  password!: string;
}
/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './newpassword.html',
  styleUrls: ['../css/auth.scss'],
})
export class NewPasswordComponent implements CognitoCallback {
  form!: FormGroup;
  submitted = false;
  @ViewChild('frame') frame!: HeaderFrameComponent;

  registrationUser = new NewPasswordUser();

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.onInit();
  }
  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    throw new Error('Method not implemented.');
  }

  onInit() {
    this.form = this.formBuilder.group({
      signupEmail: [null, [Validators.required, Validators.email]],
      existingPassword: [null, [Validators.required]],
      signupPassword: [null, [Validators.required]],
    });
  }

  ngOnInit() {
    console.log(
      'Checking if the user is already authenticated. If so, then redirect to the secure site'
    );
    this.authService.isAuthenticated(this);
  }

  onRegister() {
    this.submitted = true;
    if (this.form.valid) {
      this.registrationUser.username = this.form.get('signupEmail')?.value;
      this.registrationUser.existingPassword =
        this.form.get('existingPassword')?.value;
      this.registrationUser.password = this.form.get('signupPassword')?.value;
      console.log(this.registrationUser);
      this.authService.newPassword(this.registrationUser, this);
    }
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
      //error
      this.frame.showAlert(message, AlertTypes.DANGER);
    } else {
      //success
      //move to the next step
      this.router.navigate([this.tenantURL + environment.ROUTE_INITIAL]);
    }
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) this.router.navigate([this.tenantURL + environment.ROUTE_INITIAL]);
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
