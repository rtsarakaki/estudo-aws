import { AppGlobal } from './../../../app.global';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

import { AlertTypes } from '../../../shared/controls/alert/modal.service';
import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
} from '../../services/auth.service';
import { HeaderFrameComponent } from '../control/header-frame/header-frame.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['../css/auth.scss'],
})
export class SignInComponent implements CognitoCallback, OnInit {
  form!: FormGroup;
  submitted = false;

  mfaStep = false;
  tipagem: any = '';
  mfaData = {
    destination: '',
    callback: this.tipagem,
  };

  @ViewChild('frame') frame!: HeaderFrameComponent;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    //this.authService.logout();

    AppGlobal.tenantURL = this.activatedRoute.snapshot.params['tenant'];

    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(16),
          Validators.pattern(environment.REGEX_PASSWORD),
        ],
      ],
    });
  }

  onLogin() {
    this.submitted = true;
    if (this.form.valid) {
      this.authService.authenticate(
        this.form.get('email')?.value,
        this.form.get('password')?.value,
        this
      );
    }
  }

  cognitoCallback(message: any, result: any): void {
    if (message != null) {
      //error
      this.frame.showAlert(message, AlertTypes.DANGER);
      if (message === 'User is not confirmed.') {
        this.router.navigate([
          AppGlobal.tenantURL + '/auth/confirmRegistration',
          this.form.get('email')?.value,
        ]);
      } else if (message === 'User needs to set password.') {
        console.log('redirecting to set new password');
        this.router.navigate([AppGlobal.tenantURL + '/auth/newPassword']);
      }
    } else {
      //success
      //this.ddb.writeLogEntry('login');
      console.log('Login realizado com sucesso.');
      this.router.navigate([
        AppGlobal.tenantURL + '/' + environment.ROUTE_INITIAL,
      ]);
    }
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    this.mfaStep = true;
    this.mfaData.destination = challengeParameters.CODE_DELIVERY_DESTINATION;
    this.mfaData.callback = (code: string) => {
      console.log(code);
      if (code == null || code.length === 0) {
        this.frame.showAlert(
          'Por favor, informe o código de confirmação!',
          AlertTypes.INFO
        );
        return;
      }
      callback(code);
    };
  }

  cancelMFA(): boolean {
    this.mfaStep = false;
    return false; //necessary to prevent href navigation
  }

  get tenantURL() {
    return AppGlobal.tenantURL;
  }
}
