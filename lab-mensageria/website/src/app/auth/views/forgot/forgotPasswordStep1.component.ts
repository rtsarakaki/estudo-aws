import { AppGlobal } from './../../../app.global';
import { HeaderFrameComponent } from './../control/header-frame/header-frame.component';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';

import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
} from '../../services/auth.service';

@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './forgotPasswordStep1.html',
  styleUrls: ['../css/auth.scss'],
})
export class ForgotPasswordStep1Component implements CognitoCallback {
  form!: FormGroup;
  submitted = false;

  @ViewChild('frame') frame!: HeaderFrameComponent;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
    });
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    throw new Error('Method not implemented.');
  }

  onNext() {
    this.authService.forgotPassword(this.form.get('email')?.value, this);
  }

  cognitoCallback(message: string, result: any) {
    if (message == null && result == null) {
      //error
      this.router.navigate([
        this.tenantURL + '/auth/forgotPassword',
        this.form.get('email')?.value,
      ]);
    } else {
      //success
      this.frame.showAlert(message, AlertTypes.DANGER);
    }
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
