import { AppGlobal } from './../../../app.global';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';
import { environment } from 'src/environments/environment';

import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
} from '../../services/auth.service';
import { HeaderFrameComponent } from './../control/header-frame/header-frame.component';

@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './forgotPasswordStep2.html',
  styleUrls: ['../css/auth.scss'],
})
export class ForgotPasswordStep2Component
  implements CognitoCallback, OnInit, OnDestroy
{
  form!: FormGroup;
  submitted = false;

  @ViewChild('frame') frame!: HeaderFrameComponent;

  // verificationCode!: string;
  // email!: string;
  // password!: string;
  // errorMessage!: string | null;
  private sub: any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public authService: AuthService
  ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.form = this.formBuilder.group({
        email: [params['email']],
        verificationCode: [null, [Validators.required]],
        newPassword: [
          null,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(16),
            Validators.pattern(environment.REGEX_PASSWORD),
          ],
        ],
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onNext() {
    this.submitted = true;
    if (this.form.valid) {
      this.authService.confirmNewPassword(
        this.form.get('email')?.value,
        this.form.get('verificationCode')?.value,
        this.form.get('newPassword')?.value,
        this
      );
    }
  }

  cognitoCallback(message: string) {
    if (message != null) {
      //error
      this.frame.showAlert(message, AlertTypes.DANGER);
    } else {
      //success
      this.router.navigate([this.tenantURL + '/auth/signin']);
    }
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    throw new Error('Method not implemented.');
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
