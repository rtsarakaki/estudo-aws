import { AppGlobal } from './../../../app.global';
import { AlertTypes } from './../../../shared/controls/alert/modal.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { environment } from 'src/environments/environment';

import {
  AuthService,
  ChallengeParameters,
  CognitoCallback,
  LoggedInCallback,
} from '../../services/auth.service';
import { HeaderFrameComponent } from '../control/header-frame/header-frame.component';

@Component({
  selector: 'awscognito-angular2-app',
  template: '',
})
export class LogoutComponent implements LoggedInCallback {
  constructor(public router: Router, public authService: AuthService) {
    this.authService.isAuthenticated(this);
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      this.authService.logout();
      this.router.navigate([environment.ROUTE_INITIAL]);
    }

    this.router.navigate([environment.ROUTE_INITIAL]);
  }
}

@Component({
  selector: 'awscognito-angular2-app',
  templateUrl: './confirmRegistration.html',
  styleUrls: ['../css/auth.scss'],
})
export class ConfirmRegistrationComponent
  implements CognitoCallback, OnInit, OnDestroy
{
  form!: FormGroup;
  submitted = false;
  @ViewChild('frame') frame!: HeaderFrameComponent;

  private sub: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.form = this.formBuilder.group({
        email: [params['username'], [Validators.required, Validators.email]],
        confirmationCode: [null, [Validators.required]],
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onConfirmRegistration() {
    this.submitted = true;
    if (this.form.valid) {
      this.authService.confirmRegistration(
        this.form.get('email')?.value,
        this.form.get('confirmationCode')?.value,
        this
      );
    }
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
      //error
      this.frame.showAlert(message, AlertTypes.DANGER);
    } else {
      //success
      //move to the next step
      this.router.navigate([this.tenantURL + environment.ROUTE_INITIAL]);
    }
  }

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void {
    throw new Error('Method not implemented.');
  }

  get tenantURL() {
    return AppGlobal.tenantURL;
  }
}
