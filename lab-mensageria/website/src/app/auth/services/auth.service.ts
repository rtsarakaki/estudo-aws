import { Injectable } from '@angular/core';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
  CognitoUserSession,
} from 'amazon-cognito-identity-js';
import * as CognitoIdentity from 'aws-sdk/clients/cognitoidentity';
import * as STS from 'aws-sdk/clients/sts';
import * as AWS from 'aws-sdk/global';
import * as awsservice from 'aws-sdk/lib/service';
import { environment } from 'src/environments/environment';

export class RegistrationUser {
  name!: string;
  email!: string;
  phoneNumber!: string;
  password!: string;
}

export class NewPasswordUser {
  username!: string;
  existingPassword!: string;
  password!: string;
}

export interface CognitoCallback {
  cognitoCallback(message: any, result: any): void;

  handleMFAStep(
    challengeName: string,
    challengeParameters: ChallengeParameters,
    callback: (confirmationCode: string) => any
  ): void;
}

export interface LoggedInCallback {
  isLoggedIn(message: string, loggedIn: boolean): void;
}

export interface ChallengeParameters {
  CODE_DELIVERY_DELIVERY_MEDIUM: string;

  CODE_DELIVERY_DESTINATION: string;
}

export interface Callback {
  callback(): void;

  callbackWithParam(result: any): void;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  static currentTenant: any;
  static isLoggedin = false;
  static userSession: CognitoUser | null;
  public REGION = environment.AWS_REGION;

  public POOL_DATA: any = {
    UserPoolId: environment.AWS_COGNITO_USER_POOL_ID,
    ClientId: environment.AWS_COGNITO_CLIENT_ID,
  };

  public cognitoCreds!: AWS.CognitoIdentityCredentials;

  constructor() {}

  getUserPool() {
    if (environment.AWS_COGNITO_IDP_ENDPOINT) {
      this.POOL_DATA.endpoint = environment.AWS_COGNITO_IDP_ENDPOINT;
    }
    return new CognitoUserPool(this.POOL_DATA);
  }

  buildCognitoCreds(idTokenJwt: string) {
    let url =
      'cognito-idp.' +
      this.REGION.toLowerCase() +
      '.amazonaws.com/' +
      environment.AWS_COGNITO_USER_POOL_ID;

    if (environment.AWS_COGNITO_IDP_ENDPOINT) {
      url =
        environment.AWS_COGNITO_IDP_ENDPOINT +
        '/' +
        environment.AWS_COGNITO_USER_POOL_ID;
    }

    let logins: CognitoIdentity.LoginsMap = {};
    logins[url] = idTokenJwt;

    let params = {
      IdentityPoolId: environment.AWS_IDENTITY_POOL_ID /* required */,
      Logins: logins,
    };

    let serviceConfigs = <awsservice.ServiceConfigurationOptions>{};
    if (environment.AWS_COGNITO_IDENTITY_ENDPOINT) {
      serviceConfigs.endpoint = environment.AWS_COGNITO_IDENTITY_ENDPOINT;
    }

    let creds = new AWS.CognitoIdentityCredentials(params, serviceConfigs);
    this.setCognitoCreds(creds);

    return creds;
  }

  // AWS Stores Credentials in many ways, and with TypeScript this means that
  // getting the base credentials we authenticated with from the AWS globals gets really murky,
  // having to get around both class extension and unions. Therefore, we're going to give
  // developers direct access to the raw, unadulterated CognitoIdentityCredentials
  // object at all times.
  setCognitoCreds(creds: AWS.CognitoIdentityCredentials) {
    this.cognitoCreds = creds;
  }

  getCurrentUser() {
    return this.getUserPool().getCurrentUser();
  }

  getAccessToken(callback: Callback): void {
    if (callback == null) {
      throw 'CognitoUtil: callback in getAccessToken is null...returning';
    }
    if (this.getCurrentUser() != null) {
      this.getCurrentUser()!.getSession(function (err: any, session: any) {
        if (err) {
          console.log("CognitoUtil: Can't set the credentials:" + err);
          callback.callbackWithParam(null);
        } else {
          if (session.isValid()) {
            callback.callbackWithParam(session.getAccessToken().getJwtToken());
          }
        }
      });
    } else {
      callback.callbackWithParam(null);
    }
  }

  getIdToken(callback: Callback): void {
    if (callback == null) {
      throw 'CognitoUtil: callback in getIdToken is null...returning';
    }
    if (this.getCurrentUser() != null)
      this.getCurrentUser()!.getSession(function (err: any, session: any) {
        if (err) {
          console.log("CognitoUtil: Can't set the credentials:" + err);
          callback.callbackWithParam(null);
        } else {
          if (session.isValid()) {
            callback.callbackWithParam(session.getIdToken().getJwtToken());
          } else {
            console.log(
              "CognitoUtil: Got the id token, but the session isn't valid"
            );
          }
        }
      });
    else callback.callbackWithParam(null);
  }

  getCognitoIdentity(): string {
    return this.cognitoCreds.identityId;
  }

  private onLoginSuccess = (
    callback: CognitoCallback,
    session: CognitoUserSession
  ) => {
    AWS.config.credentials = this.buildCognitoCreds(
      session.getIdToken().getJwtToken()
    );

    // So, when CognitoIdentity authenticates a user, it doesn't actually hand us the IdentityID,
    // used by many of our other handlers. This is handled by some sly underhanded calls to AWS Cognito
    // API's by the SDK itself, automatically when the first AWS SDK request is made that requires our
    // security credentials. The identity is then injected directly into the credentials object.
    // If the first SDK call we make wants to use our IdentityID, we have a
    // chicken and egg problem on our hands. We resolve this problem by "priming" the AWS SDK by calling a
    // very innocuous API call that forces this behavior.
    let clientParams: any = {};
    if (environment.AWS_STS_ENDPOINT) {
      clientParams.endpoint = environment.AWS_STS_ENDPOINT;

      let sts = new STS(clientParams);

      sts.getCallerIdentity(function (err: any, data: any) {
        callback.cognitoCallback(null, session);
      });
    } else {
      callback.cognitoCallback(null, session);
    }
  };

  authenticate(username: string, password: string, callback: CognitoCallback) {
    let authenticationData = {
      Username: username,
      Password: password,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);

    let userData = {
      Username: username,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);

    AuthService.userSession = cognitoUser;

    cognitoUser.authenticateUser(authenticationDetails, {
      newPasswordRequired: (userAttributes, requiredAttributes) =>
        callback.cognitoCallback(`User needs to set password.`, null),
      onSuccess: (result) => this.onLoginSuccess(callback, result),
      onFailure: (err) => this.onLoginError(callback, err),
      mfaRequired: (challengeName, challengeParameters) => {
        callback.handleMFAStep(
          challengeName,
          challengeParameters,
          (confirmationCode: string) => {
            cognitoUser.sendMFACode(confirmationCode, {
              onSuccess: (result) => {
                AuthService.isLoggedin = true;
                this.onLoginSuccess(callback, result);
              },
              onFailure: (err) => this.onLoginError(callback, err),
            });
          }
        );
      },
    });
  }

  isLoggedIn(): boolean {
    var isAuth = false;

    let poolData = {
      UserPoolId: environment.AWS_COGNITO_USER_POOL_ID,
      ClientId: environment.AWS_COGNITO_CLIENT_ID,
    };

    var userPool = new CognitoUserPool(poolData);
    var cognitoUser = userPool.getCurrentUser();

    if (cognitoUser != null) {
      cognitoUser.getSession((err: any, session: any) => {
        if (err) {
          alert(err.message || JSON.stringify(err));
        }
        AuthService.userSession = cognitoUser;
        isAuth = session.isValid();
      });
    }
    return isAuth;
  }

  private onLoginError = (callback: CognitoCallback, err: any) => {
    callback.cognitoCallback(err.message, null);
  };

  register(user: RegistrationUser, callback: CognitoCallback): void {
    let attributeList = [];

    let dataEmail = {
      Name: 'email',
      Value: user.email,
    };
    let dataName = {
      Name: 'name',
      Value: user.name,
    };
    let dataPhoneNumber = {
      Name: 'phone_number',
      Value: user.phoneNumber,
    };
    attributeList.push(new CognitoUserAttribute(dataEmail));
    attributeList.push(new CognitoUserAttribute(dataName));
    attributeList.push(new CognitoUserAttribute(dataPhoneNumber));

    this.getUserPool().signUp(
      user.email,
      user.password,
      attributeList,
      null,
      function (err, result) {
        if (err) {
          callback.cognitoCallback(err.message, null);
        } else {
          callback.cognitoCallback(null, result);
        }
      }
    );
  }

  update(user: RegistrationUser, callback: any) {
    let attributeList = [];

    let dataName = {
      Name: 'name',
      Value: user.name,
    };
    attributeList.push(new CognitoUserAttribute(dataName));

    if (this.isLoggedIn()) {
      AuthService.userSession?.updateAttributes(attributeList, callback);
    }
  }

  confirmRegistration(
    username: string,
    confirmationCode: string,
    callback: CognitoCallback
  ): void {
    let userData = {
      Username: username,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.confirmRegistration(
      confirmationCode,
      true,
      function (err, result) {
        if (err) {
          callback.cognitoCallback(err.message, null);
        } else {
          callback.cognitoCallback(null, result);
        }
      }
    );
  }

  resendCode(username: string, callback: CognitoCallback): void {
    let userData = {
      Username: username,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.resendConfirmationCode(function (err, result) {
      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        callback.cognitoCallback(null, result);
      }
    });
  }

  newPassword(
    newPasswordUser: NewPasswordUser,
    callback: CognitoCallback
  ): void {
    // Get these details and call
    //cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
    let authenticationData = {
      Username: newPasswordUser.username,
      Password: newPasswordUser.existingPassword,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);

    let userData = {
      Username: newPasswordUser.username,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      newPasswordRequired: function (userAttributes, requiredAttributes) {
        // User was signed up by an admin and must provide new
        // password and required attributes, if any, to complete
        // authentication.

        // the api doesn't accept this field back
        delete userAttributes.email_verified;
        cognitoUser.completeNewPasswordChallenge(
          newPasswordUser.password,
          requiredAttributes,
          {
            onSuccess: function (result) {
              callback.cognitoCallback(null, userAttributes);
            },
            onFailure: function (err) {
              callback.cognitoCallback(err, null);
            },
          }
        );
      },
      onSuccess: function (result) {
        callback.cognitoCallback(null, result);
      },
      onFailure: function (err) {
        callback.cognitoCallback(err, null);
      },
    });
  }

  forgotPassword(username: string, callback: CognitoCallback) {
    let userData = {
      Username: username,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.forgotPassword({
      onSuccess: function () {},
      onFailure: function (err) {
        callback.cognitoCallback(err.message, null);
      },
      inputVerificationCode() {
        callback.cognitoCallback(null, null);
      },
    });
  }

  confirmNewPassword(
    email: string,
    verificationCode: string,
    password: string,
    callback: CognitoCallback
  ) {
    let userData = {
      Username: email,
      Pool: this.getUserPool(),
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.confirmPassword(verificationCode, password, {
      onSuccess: function () {
        callback.cognitoCallback(null, null);
      },
      onFailure: function (err) {
        callback.cognitoCallback(err.message, null);
      },
    });
  }

  logout() {
    AuthService.isLoggedin = false;
    this.getCurrentUser()!.signOut();
  }

  isAuthenticated(callback: LoggedInCallback) {
    if (callback == null)
      throw 'UserLoginService: Callback in isAuthenticated() cannot be null';

    let cognitoUser = this.getCurrentUser();

    if (cognitoUser != null) {
      cognitoUser.getSession(function (err: any, session: any) {
        if (err) {
          console.log(
            "UserLoginService: Couldn't get the session: " + err,
            err.stack
          );
          callback.isLoggedIn(err, false);
        } else {
          AuthService.userSession = cognitoUser;
          callback.isLoggedIn(err, session.isValid());
        }
      });
    } else {
      callback.isLoggedIn("Can't retrieve the CurrentUser", false);
    }
  }
}
