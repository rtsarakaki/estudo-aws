import { Injectable } from '@angular/core';

import { AuthService, Callback } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserParametersService {
  constructor(private authService: AuthService) {}

  getParameters(callback: Callback) {
    let cognitoUser = this.authService.getCurrentUser();

    if (cognitoUser != null) {
      cognitoUser.getSession(function (err: any, session: any) {
        if (err)
          console.log("UserParametersService: Couldn't retrieve the user");
        else {
          cognitoUser!.getUserAttributes(function (err, result) {
            if (err) {
              console.log('UserParametersService: in getParameters: ' + err);
            } else {
              callback.callbackWithParam(result);
            }
          });
        }
      });
    } else {
      callback.callbackWithParam(null);
    }
  }
}
