import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { ConfirmRegistrationComponent } from './views/confirm/confirmRegistration.component';
import { HeaderFrameComponent } from './views/control/header-frame/header-frame.component';
import { ForgotPasswordStep1Component } from './views/forgot/forgotPasswordStep1.component';
import { ForgotPasswordStep2Component } from './views/forgot/forgotPasswordStep2.component';
import { MFAComponent } from './views/mfa/mfa.component';
import { NewPasswordComponent } from './views/newpassword/newpassword.component';
import { MyProfileComponent } from './views/profile/myprofile.component';
import { ResendCodeComponent } from './views/resend/resendCode.component';
import { SignInComponent } from './views/sign-in/sign-in.component';
import { SignUpComponent } from './views/sign-up/sign-up.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    ForgotPasswordStep1Component,
    ForgotPasswordStep2Component,
    ResendCodeComponent,
    ConfirmRegistrationComponent,
    NewPasswordComponent,
    MFAComponent,
    MyProfileComponent,
    HeaderFrameComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AuthRoutingModule,
  ],
  exports: [SignInComponent, SignUpComponent],
})
export class AuthModule {}
