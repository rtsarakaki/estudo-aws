import { AppGlobal } from './app.global';
import { TenantService } from './security/services/tenant.service';
import { Tenant } from './security/models/tenant';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CognitoUser, CognitoUserPool } from 'amazon-cognito-identity-js';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

import { AuthService } from './auth/services/auth.service';
import { Console } from 'console';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'SmaRTSuite';
  cognitoUser!: CognitoUser | null;
  subscricao!: Subscription;

  get userName() {
    return this.cognitoUser?.getUsername();
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private tenantService: TenantService
  ) {}

  ngOnInit(): void {

    let poolData = {
      UserPoolId: environment.AWS_COGNITO_USER_POOL_ID,
      ClientId: environment.AWS_COGNITO_CLIENT_ID,
    };
    let userPool = new CognitoUserPool(poolData);
    this.cognitoUser = userPool.getCurrentUser();
  }

  onLogout(): void {
    this.cognitoUser?.signOut();
    this.router.navigate([this.tenantURL + '/auth/signin']);
  }

  get isAuth() {
    //Se o Tenant não for informado na rota, não permite acesso.
    if (AppGlobal.tenantURL) {
      const loginPage =
        this.router.routerState.snapshot.url.indexOf('signin') != -1;
      const hasCurrentUser = this.authService.getCurrentUser() != null;
      return hasCurrentUser && !loginPage;
    } else {
      return false;
    }
  }

  get tenantURL() {
    return AppGlobal.tenantURL;
  }
}
