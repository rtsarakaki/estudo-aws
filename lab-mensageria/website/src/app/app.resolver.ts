import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class AppResolver implements Resolve<any> {
  constructor() {}

  resolve (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    console.log('AppResolver');
    console.log(route);
    return EMPTY;
  }
}
