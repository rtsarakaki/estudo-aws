import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/auth-guard.guard';
import { SqsUsoApisComponent } from './sqs-uso-apis/sqs-uso-apis.component';

const routes: Routes = [
  { path: '', component: SqsUsoApisComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SqsRoutingModule { }
