import { SharedModule } from './../shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SqsRoutingModule } from './sqs-routing.module';
import { SqsUsoApisComponent } from './sqs-uso-apis/sqs-uso-apis.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SqsSendMessageComponent } from './sqs-send-message/sqs-send-message.component';
import { SqsReceiveMessageComponent } from './sqs-receive-message/sqs-receive-message.component';


@NgModule({
  declarations: [
    SqsUsoApisComponent,
    SqsSendMessageComponent,
    SqsReceiveMessageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SqsRoutingModule,
    SharedModule
  ]
})
export class SqsModule { }
