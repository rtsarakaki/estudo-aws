import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { interval, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { SqsService } from './../services/sqs.service';

@Component({
  selector: 'app-sqs-receive-message',
  templateUrl: './sqs-receive-message.component.html',
  styleUrls: ['./sqs-receive-message.component.scss'],
})
export class SqsReceiveMessageComponent implements OnInit, OnDestroy {
  timmer = 30;
  form!: FormGroup;
  submitted = false;
  secondsCounter = 1;
  statusMessage = 'Aguardando ...';
  messages: any;
  waitingResult = false;
  timmerStoped = true;
  timmerSubscription!: Subscription;
  resultMessage!: any;
  resultClass = '';

  constructor(
    private formBuilder: FormBuilder,
    private sqsService: SqsService
  ) {}

  ngOnInit(): void {
    const urlRegex =
      /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

    //inicializa o formulário
    this.form = this.formBuilder.group({
      QueueUrl: [
        'https://sqs.us-east-1.amazonaws.com/417796969030/lab',
        [Validators.required, Validators.pattern(urlRegex)],
      ],
      MaxNumberOfMessages: [
        10,
        [Validators.required, Validators.min(1), Validators.max(10)],
      ],
      VisibilityTimeout: [
        90,
        [Validators.required, Validators.min(0), Validators.max(43200)],
      ],
      WaitTimeSeconds: [
        20,
        [Validators.required, Validators.min(1), Validators.max(20)],
      ],
    });
  }

  ngOnDestroy() {
    if (this.timmerSubscription) {
      this.timmerSubscription.unsubscribe();
    }
  }

  onStopTimmer() {
    if (this.timmerStoped) {
      this.resultMessage = undefined;
      this.secondsCounter = 1;
      this.timmerStoped = false;

      this.timmerSubscription = interval(1000)
        .pipe(
          map((res) => {
            if (!this.timmerStoped) {
              if (this.secondsCounter >= 1) {
                if (!this.waitingResult) {
                  this.statusMessage = 'Aguardando ...';
                }
              } else {
                this.resultClass = '';
                this.resultMessage = undefined;
                this.messages = undefined;
                this.statusMessage = `Buscando mensagens ...`;
                this.waitingResult = true;

                this.sqsService.receiveMessage(this.form.value).subscribe(
                  (success: any) => {
                    this.submitted = false;

                    // console.log('success 1');
                    // console.log(success);

                    this.waitingResult = false;
                    this.messages = success.result.Messages;
                  },
                  (error) => {
                    // console.log('error 1');
                    this.waitingResult = false;
                    this.resultClass = 'alert-danger';
                    this.resultMessage = JSON.stringify(error);
                  }
                );
                this.secondsCounter = this.timmer;
              }
              return this.secondsCounter;
            }
            return 0;
          })
        )
        .subscribe(
          (success: any) => {
            if (!this.timmerStoped) {
              // console.log('success 2');
              this.secondsCounter--;
            }
          },
          (error) => {
            if (!this.timmerStoped) {
              // console.log('error 2');
              this.secondsCounter--;
            }
          }
        );
    } else {
      this.timmerSubscription.unsubscribe();
      this.timmerStoped = true;
    }
  }

  onDelete(queueUrl: string, receiptHandle: string) {
    console.log('onDelete');
    this.statusMessage = `Apagando mensagem ...`;
    this.resultMessage = undefined;
    this.sqsService.deleteMessage(queueUrl, receiptHandle).subscribe(
      (success: any) => {
        console.log('onDelete success');
        this.resultClass = 'alert-success';
        this.resultMessage = 'Mensagem apagada com sucesso';
      },
      (error) => {
        console.log('onDelete error');
        this.resultClass = 'alert-danger';
        this.resultMessage = JSON.stringify(error);
      }
    );
  }

  onChangeMessageVisibility(
    queueUrl: string,
    receiptHandle: string,
    visibilityTimeout: number
  ) {
    console.log('onChangeMessageVisibility');
    this.statusMessage = `Liberando mensagem ...`;
    this.resultMessage = undefined;
    this.sqsService
      .changeMessageVisibility(queueUrl, receiptHandle, visibilityTimeout)
      .subscribe(
        (success: any) => {
          console.log('onChangeMessageVisibility success');
          this.resultClass = 'alert-success';
          this.resultMessage = 'Mensagem liberada com sucesso';
        },
        (error) => {
          console.log('onChangeMessageVisibility error');
          this.resultClass = 'alert-danger';
          this.resultMessage = JSON.stringify(error);
        }
      );
  }

  getConfigMessage() {
    const waitTimeSeconds = this.waitingResult
      ? 'WAIT_TIME_SECONDS'
      : 'WaitTimeSeconds';
    return `${waitTimeSeconds}:  ${this.form.get('WaitTimeSeconds')?.value} -
    MaxNumberOfMessages: ${this.form.get('MaxNumberOfMessages')?.value} -
    VisibilityTimeout:  ${this.form.get('VisibilityTimeout')?.value}
    `;
  }
}
