import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { environment } from './../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class SqsService {
  // private readonly API = `${environment.API}aws-sqs/message`;

  constructor(protected http: HttpClient) {}

  receiveMessage(value: any) {
    return this.http.post(`${environment.API}aws-sqs/receive-message`, value).pipe(first());
  }

  sendMessage(value: any) {
    return this.http.post(`${environment.API}aws-sqs/send-message`, value).pipe(first());
  }

  deleteMessage(queueUrl: string, receiptHandle: string) {
    const params = {
      QueueUrl: queueUrl,
      ReceiptHandle: receiptHandle
    }
    console.log(params);
    return this.http.post(`${environment.API}aws-sqs/delete-message`, params).pipe(first());
  }

  changeMessageVisibility(queueUrl: string, receiptHandle: string, visibilityTimeout: number) {
    const params = {
      QueueUrl: queueUrl,
      ReceiptHandle: receiptHandle,
      VisibilityTimeout: visibilityTimeout
    }
    console.log(params);
    return this.http.post(`${environment.API}aws-sqs/change-message-visibility`, params).pipe(first());
  }
}
