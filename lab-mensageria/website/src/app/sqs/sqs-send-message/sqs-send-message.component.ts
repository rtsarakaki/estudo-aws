import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';

import { ModalService } from './../../shared/controls/alert/modal.service';
import { SqsService } from './../services/sqs.service';

@Component({
  selector: 'app-sqs-send-message',
  templateUrl: './sqs-send-message.component.html',
  styleUrls: ['./sqs-send-message.component.scss'],
})
export class SqsSendMessageComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  queueFIFO = false;

  resultMessage!: any;
  resultClass = '';

  constructor(
    private formBuilder: FormBuilder,
    private sqsService: SqsService,
    private alertModalService: ModalService
  ) {}

  ngOnInit(): void {
    const urlRegex =
      /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

    //inicializa o formulário
    this.form = this.formBuilder.group({
      QueueUrl: ['https://sqs.us-east-1.amazonaws.com/417796969030/lab', [Validators.required, Validators.pattern(urlRegex)]],
      MessageBody: [`{
        "Title": {
          "DataType": "String",
          "StringValue": "The Whistler"
        },
        "Author": {
          "DataType": "String",
          "StringValue": "John Grisham"
        },
        "WeeksOn": {
          "DataType": "Number",
          "StringValue": "6"
        }`, [Validators.required]],
      DelaySeconds: [0, [Validators.required]],
      MessageAttributes: [null],
      MessageDeduplicationId: [null],
      MessageGroupId: [null],
      MessageSystemAttributes: [null],
    });

    //monitora mudanças no campo QueueUrl para verificar se é uma fila FIFO ou Standard
    const FIFO = 'FIFO';
    this.form
      .get('QueueUrl')
      ?.valueChanges.pipe(map((value: string) => value.indexOf(FIFO) !== -1))
      .subscribe((value) => this.defineQueueType(value));
  }

  onSendMessage() {
    this.submitted = true;
    if (this.form.valid) {
      this.sqsService.sendMessage(this.form.value).subscribe(
        (success) => {
          this.submitted = false;
          this.resultClass = 'alert-success';
          this.resultMessage = JSON.stringify('Mensagem enviada com sucesso.');
          // this.alertModalService.showAlert(
          //   'Mensagem enviada com sucesso.',
          //   AlertTypes.SUCCESS,
          //   3000
          // );
        },
        (error) => {
          this.resultClass = 'alert-danger';
          this.resultMessage = JSON.stringify(error);
          // this.alertModalService.showAlert(
          //   'Erro ao tentar gravar enviar a mensagem. Tente novamente.',
          //   AlertTypes.DANGER
          // );
        }
      );
    }
  }

  defineQueueType(value: boolean) {
    this.queueFIFO = value;
  }

  getFieldValue(fieldName: string) {
    return this.form.get(fieldName)?.value;
  }

  getSumaryMessage() {
    if (this.getFieldValue('QueueUrl')) {
      return `Gerar mensagem na fila ${this.getFieldValue('QueueUrl')} ${
        this.getFieldValue('DelaySeconds') > 0
          ? `que será entregue após ${this.getFieldValue(
              'DelaySeconds'
            )} segundos para os consumidores da fila`
          : ''
      }`;
    } else {
      return '';
    }
  }
}
