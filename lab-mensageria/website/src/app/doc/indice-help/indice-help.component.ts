import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-indice-help',
  templateUrl: './indice-help.component.html',
  styleUrls: ['./indice-help.component.scss']
})
export class IndiceHelpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  scrollToElement($element: any): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

}
