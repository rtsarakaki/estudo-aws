import { AppPublicComponent } from './app-public/app-public.component';
import { AppResolver } from './app.resolver';
import { AppPageNotFoundComponent } from './app-page-not-found/app-page-not-found.component';
import { AppHomeComponent } from './app-home/app-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth-guard.guard';
import { IndiceHelpComponent } from './doc/indice-help/indice-help.component';

const routes: Routes = [
  {
    path: '',
    component: AppPublicComponent,
  },
  {
    path: ':tenant',
    component: AppHomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':tenant/sqs',
    loadChildren: () => import('./sqs/sqs.module').then((m) => m.SqsModule),
    canActivate: [AuthGuard],
  },
  {
    path: ':tenant/security',
    loadChildren: () =>
      import('./security/security.module').then((m) => m.SecurityModule),
    canActivate: [AuthGuard],
  },
  {
    path: ':tenant/auth',
    loadChildren: () =>
      import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: ':tenant/doc',
    component: IndiceHelpComponent,
    canActivate: [AuthGuard],
  },
{
    path: '**',
    component: AppPageNotFoundComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
