import { CardMenu, CardMenuApp } from './../../models/cardMenu';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card-menu-preview',
  templateUrl: './card-menu-preview.component.html',
  styleUrls: ['./card-menu-preview.component.scss']
})
export class CardMenuPreviewComponent implements OnInit {
  @Input() cardMenu = new CardMenu();
  @Input() alert!: AlertDismissibleComponent;
  @Input() selectedApp!: CardMenuApp;
  @Output() onSelectApp = new EventEmitter<CardMenuApp>();

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(app: CardMenuApp) {
    app.sort = app.text;
    this.onSelectApp.emit(app);
  }

}
