import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Tenant } from '../../models/tenant';
import { TenantService } from '../../services/tenant.service';
import { FormEditBase } from './../../../shared/framework/view/form-edit-base';

@Component({
  selector: 'app-tenant-edit',
  templateUrl: './tenant-edit.component.html',
  styleUrls: ['./tenant-edit.component.scss'],
})
export class TenantEditComponent
  extends FormEditBase<Tenant>
  implements OnInit
{
  constructor(
    private formBuilder: FormBuilder,
    activatedRoute: ActivatedRoute,
    tenantService: TenantService
  ) {
    super(activatedRoute);
    this.service = tenantService;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.form = this.formBuilder.group({
      name: [
        this.item.name,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      route: [
        this.item.route,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),
        ],
      ],
    });
  }
}
