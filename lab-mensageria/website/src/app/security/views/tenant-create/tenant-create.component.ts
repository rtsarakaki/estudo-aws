import { AppGlobal } from './../../../app.global';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TenantService } from '../../services/tenant.service';
import { FormEditBase } from './../../../shared/framework/view/form-edit-base';
import { Tenant } from './../../models/tenant';

@Component({
  selector: 'app-tenant-create',
  templateUrl: './tenant-create.component.html',
  styleUrls: ['./tenant-create.component.scss'],
})
export class TenantCreateComponent
  extends FormEditBase<Tenant>
  implements OnInit
{
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    activatedRoute: ActivatedRoute,
    tenantService: TenantService
  ) {
    super(activatedRoute);
    this.service = tenantService;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      route: [
        null,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),
        ],
      ],
    });
  }

  onNavigate() {
    this.router.navigate([this.tenantURL + '/security/tenant/', this.item.sortedKey]);
  }

  get tenantURL() {
    return '/' + AppGlobal.tenantURL;
  }
}
