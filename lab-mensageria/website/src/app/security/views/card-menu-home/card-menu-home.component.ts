import { CardMenuCreateComponent } from './../card-menu-create/card-menu-create.component';
import { CardMenuAppCreateComponent } from './../card-menu-app-create/card-menu-app-create.component';
import { CardMenu, CardMenuApp } from './../../models/cardMenu';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card-menu-home',
  templateUrl: './card-menu-home.component.html',
  styleUrls: ['./card-menu-home.component.scss']
})
export class CardMenuHomeComponent implements OnInit {
  inscricao!: Subscription;
  deleted = false;
  cardMenu = new CardMenu();
  isEditMode = false;
  @ViewChild('alert') alert!: AlertDismissibleComponent;
  @ViewChild('cardMenuCreate') cardMenuCreate!: CardMenuCreateComponent;
  @ViewChild('cardMenuAppCreate') cardMenuAppCreate!: CardMenuAppCreateComponent;

  @Input() selectedApp!: CardMenuApp;

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.inscricao = this.activatedRoute.data.subscribe((info) => {
      if (info.dados) {
        this.cardMenu = info.dados.data;
        this.deleted = false;
        this.isEditMode = true;
      }
      else {
        console.log('sem dados');
      }
    });
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  onSelectApp(app: CardMenuApp) {
    this.cardMenuAppCreate.selectedApp = app;
  }
}
