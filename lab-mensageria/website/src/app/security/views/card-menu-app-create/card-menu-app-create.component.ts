import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';

import { ModalService } from './../../../shared/controls/alert/modal.service';
import { CardMenu, CardMenuApp } from './../../models/cardMenu';

@Component({
  selector: 'app-card-menu-app-create',
  templateUrl: './card-menu-app-create.component.html',
  styleUrls: ['./card-menu-app-create.component.scss'],
})
export class CardMenuAppCreateComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  newApp = new CardMenuApp();
  @Input() cardMenu = new CardMenu();
  @Input() alert!: AlertDismissibleComponent;
  isEditMode = false;
  deleted = false;

  @Input() set selectedApp(value: CardMenuApp | null) {
    console.log(value);
    this.deleted = false;
    if (value) {
      this.newApp = value;
      this.isEditMode = true;
    } else {
      this.newApp = new CardMenuApp();

      this.newApp.icon = 'person_add';
      this.newApp.routerLink = 'menu';
      this.newApp.buttonClass = 'btn-primary';
      this.newApp.textClass = 'text-light';

      this.isEditMode = false;
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    if (this.cardMenu.apps[0]) {
      this.selectedApp = this.cardMenu.apps[0];
    } else {
      this.newApp.icon = 'person_add';
      this.newApp.routerLink = 'menu';
      this.newApp.buttonClass = 'btn-primary';
      this.newApp.textClass = 'text-light';
    }

    this.form = this.formBuilder.group({
      text: ['app', [Validators.required]],
      icon: ['account_circle', [Validators.required]],
      routerLink: ['menu', [Validators.required]],
      buttonClass: ['bg-primary'],
      textClass: ['text-light'],
    });
  }

  onCreate() {
    if (this.form.valid) {
      this.newApp.timestamp = Date.now();
      this.newApp.sort = this.newApp.text;
      this.cardMenu.apps.push(Object.assign({}, this.newApp));

      console.log(this.cardMenu.apps);
      this.newApp.timestamp = 0;
      this.newApp.text = '';
      this.newApp.routerLink = '';
      this.newApp.icon = '';
    }
  }

  removeItem() {
    this.selectedApp;
    this.cardMenu.apps.forEach((value, index) => {
      if (value.timestamp === this.newApp?.timestamp) {
        this.cardMenu.apps.splice(index, 1);
        this.deleted = true;
      }
    });

    if (this.cardMenu.apps.length === 0) {
      this.isEditMode = false;
      this.newApp.text = '';
      this.newApp.routerLink = '';
      this.newApp.icon = '';
    }
  }

  onDelete() {
    const result$ = this.modalService.showConfirm(
      'Apagar aplicação',
      'Por favor, confirme se realmente deseja apagar a aplciação.',
      'Confirmar',
      'Desistir'
    );
    result$
      .asObservable()
      .pipe(
        take(1),
        map((result: any) => (result ? this.removeItem() : EMPTY))
      )
      .subscribe(
        (success) => {
          if (this.deleted) {
            this.alert.show(
              'Sucesso!',
              'Item removido da lista de aplicações, mas para efetivar as alterações você precisa confimar a alteração da caixa de aplicações.',
              AlertTypes.SUCCESS
            );
          }
        },
        (error) => {
          this.alert.show('Oops, algo deu errado.', error, AlertTypes.DANGER);
        },
        () => console.log('request completo')
      );
  }

  onStartAdd() {
    this.selectedApp = null;
  }
}
