import { AppGlobal } from './../../../app.global';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Tenant } from '../../models/tenant';
import { FormEditBase } from './../../../shared/framework/view/form-edit-base';
import { TenantService } from './../../services/tenant.service';


@Component({
  selector: 'app-tenant-info',
  templateUrl: './tenant-info.component.html',
  styleUrls: ['./tenant-info.component.scss'],
})
export class TenantInfoComponent extends FormEditBase<Tenant> implements OnInit {

  constructor(
    private router: Router,
    activatedRoute: ActivatedRoute,
    public tenantService: TenantService
  ) {
    super(activatedRoute)
  }

  onEdit() {
    this.router.navigate([this.tenantURL + '/tenant/edit', this.item.sortedKey]);
  }

  get tenantURL() {
    return AppGlobal.tenantURL;
  }
}
