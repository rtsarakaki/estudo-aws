import { take, switchMap } from 'rxjs/operators';
import { ModalService } from './../../../shared/controls/alert/modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';

import { CardMenu } from './../../models/cardMenu';
import { CardMenuService } from './../../services/card-menu.service';
import { EMPTY } from 'rxjs';

@Component({
  selector: 'app-card-menu-create',
  templateUrl: './card-menu-create.component.html',
  styleUrls: ['./card-menu-create.component.scss'],
})
export class CardMenuCreateComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  deleted = false;
  @Input() cardMenu = new CardMenu();
  @Input() alert!: AlertDismissibleComponent;
  @Input() isEditMode!: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private cardMenuService: CardMenuService,
    private router: Router,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [
        this.cardMenu.name,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      context: [this.cardMenu.context],
      order: [this.cardMenu.order, [Validators.required, Validators.min(0)]],
      headerClass: [this.cardMenu.headerClass],
    });
  }

  onCreate() {
    this.submitted = true;
    if (this.form.valid) {
      this.cardMenuService.create(this.cardMenu).subscribe(
        (success) => {
          this.submitted = false;
          this.cardMenu = <CardMenu>success;
          this.alert.show(
            'Sucesso!',
            'Caixa criada com sucesso!',
            AlertTypes.SUCCESS
          );
        },
        (error) => {
          this.alert.show(
            'Oops, alog deu errado!',
            error.message,
            AlertTypes.DANGER
          );
        }
      );
    }
  }

  onUpdate() {
    this.submitted = true;
    if (this.form.valid) {
      this.cardMenuService.update(this.cardMenu).subscribe(
        (success) => {
          let obj: any = success;
          console.log(success);
          this.submitted = false;
          this.cardMenu = <CardMenu>obj.data;
          this.alert.show(
            'Sucesso!',
            'Caixa alterada com sucesso!',
            AlertTypes.SUCCESS
          );
        },
        (error) => {
          this.alert.show(
            'Oops, alog deu errado!',
            error.message,
            AlertTypes.DANGER
          );
        }
      );
    }
  }

  onDelete() {
    if (this.cardMenu.apps.length === 0) {
      const result$ = this.modalService.showConfirm(
        'Apagar menu',
        'Por favor, confirme se realmente deseja apagar o menu.',
        'Confirmar',
        'Desistir'
      );
      result$
        .asObservable()
        .pipe(
          take(1),
          switchMap((result) =>
            result
              ? this.cardMenuService.delete(this.cardMenu.sortedKey)
              : EMPTY
          )
        )
        .subscribe(
          (success) => {
            this.deleted = true;
            this.alert.show(
              'Sucesso!',
              'Caixa de aplicações apagada com sucesso.',
              AlertTypes.SUCCESS
            );
          },
          (error) => {
            this.alert.show('Oops, algo deu errado.', error, AlertTypes.DANGER);
          },
          () => console.log('request completo')
        );
    } else {
      this.alert.show('Informação!', 'Antes de excluir a caixa de aplicações, por favor remova todas aplicações.', AlertTypes.INFO);
    }
  }

  onNavigate() {
    this.router.navigate(['menu/', this.cardMenu.sortedKey]);
  }
}
