import { AppGlobal } from './../../../app.global';
import { Component, OnInit } from '@angular/core';

import { CardMenuService } from './../../services/card-menu.service';

@Component({
  selector: 'app-security-home',
  templateUrl: './security-home.component.html',
  styleUrls: ['./security-home.component.scss']
})
export class SecurityHomeComponent implements OnInit {
  apps: any;

  constructor(private cardMenuService: CardMenuService) { }

  ngOnInit(): void {
    this.listMenus()
  }

  listMenus() {
    this.cardMenuService.getByContext(this.constructor.name).subscribe(
      (success: any) => {
        console.log(success.data);
        this.apps = success.data[0].apps;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getRouterLink(newRoute: string) {
    return '/' + AppGlobal.tenantURL + newRoute;
  }

}
