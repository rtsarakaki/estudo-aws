import { AppGlobal } from './../../../app.global';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TenantService } from './../../services/tenant.service';

@Component({
  selector: 'app-tenant-home',
  templateUrl: './tenant-home.component.html',
  styleUrls: ['./tenant-home.component.scss'],
})
export class TenantHomeComponent implements OnInit {
  constructor(private router: Router, public tenantService: TenantService, private route: ActivatedRoute) {}

  ngOnInit(): void {}

  onCreate() {
    this.router.navigate([AppGlobal.tenantURL + '/security/tenant/create']);
  }

  onSelectItem(item: any) {
    this.router.navigate([AppGlobal.tenantURL + '/security/tenant/', item.sortedKey]);
  }
}
