import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';

import { CardMenu } from '../models/cardMenu';
import { CardMenuService } from './../services/card-menu.service';

@Injectable({
  providedIn: 'root',
})
export class CardMenuHomeResolver implements Resolve<CardMenu> {
  constructor(private cardMenuService: CardMenuService) {}

  resolve (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<CardMenu> | Promise<CardMenu> | CardMenu {
    let sortedKey = route.params['sortedKey'];
    if (sortedKey) {
      return <CardMenu>(<unknown>this.cardMenuService.getById(sortedKey));
    }
    return EMPTY;
  }
}
