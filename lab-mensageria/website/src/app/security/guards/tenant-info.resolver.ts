import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';

import { Tenant } from '../models/tenant';
import { TenantService } from '../services/tenant.service';

@Injectable({
  providedIn: 'root',
})
export class TenantInfoResolver implements Resolve<Tenant> {
  constructor(private tenantService: TenantService) {}

  resolve (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Tenant> | Promise<Tenant> | Tenant {
    let sortedKey = route.params['sortedKey'];
    if (sortedKey) {
      return <Tenant>(<unknown>this.tenantService.getById(sortedKey));
    }
    return EMPTY;
  }
}
