import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment.prod';
import { CrudServiceBase } from '../../shared/framework/service/crud-service-base';
import { CardMenu } from '../models/cardMenu';

@Injectable({
  providedIn: 'root',
})
export class CardMenuService extends CrudServiceBase<CardMenu> {

  constructor(protected http: HttpClient) {
    super(http, `${environment.API}security/card-menu`);
  }
}
