import { Tenant } from './../models/tenant';
import { CrudServiceBase } from '../../shared/framework/service/crud-service-base';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class TenantService extends CrudServiceBase<Tenant> {

  constructor(protected http: HttpClient) {
    super(http, `${environment.API}security/tenant`);
  }
}
