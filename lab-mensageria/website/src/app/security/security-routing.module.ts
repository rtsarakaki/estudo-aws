import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/auth-guard.guard';
import { CardMenuHomeResolver } from './guards/card-menu-info.resolver';
import { TenantInfoResolver } from './guards/tenant-info.resolver';
import { CardMenuHomeComponent } from './views/card-menu-home/card-menu-home.component';
import { SecurityHomeComponent } from './views/security-home/security-home.component';
import { TenantCreateComponent } from './views/tenant-create/tenant-create.component';
import { TenantEditComponent } from './views/tenant-edit/tenant-edit.component';
import { TenantHomeComponent } from './views/tenant-home/tenant-home.component';
import { TenantInfoComponent } from './views/tenant-info/tenant-info.component';

const routes: Routes = [
  {
    path: '',
    component: SecurityHomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'tenant',
        component: TenantHomeComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: 'create',
            component: TenantCreateComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'edit/:sortedKey',
            component: TenantEditComponent,
            canActivate: [AuthGuard],
            resolve: { dados: TenantInfoResolver },
          },
          {
            path: ':sortedKey',
            component: TenantInfoComponent,
            canActivate: [AuthGuard],
            resolve: { dados: TenantInfoResolver },
          },
        ],
      },
      { path: '', redirectTo: 'tenant', pathMatch: 'full' },
    ],
  },
  {
    path: 'card-menu-home',
    component: CardMenuHomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'card-menu-home/:sortedKey',
    component: CardMenuHomeComponent,
    canActivate: [AuthGuard],
    resolve: { dados: CardMenuHomeResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {}
