export class CardMenu {
  constructor() {}

  partitionKey!: string;
  sortedKey!: string;
  name!: string;
  context!: string;
  order!: number;
  headerClass!: string;
  created!: string;
  modified!: string;
  apps: Array<CardMenuApp> = [];
  sort!: string;
}

export class CardMenuApp {
  constructor() {}

  text!: string;
  icon!: string;
  routerLink!: string;
  buttonClass!: string;
  textClass!: string;
  timestamp!: number;
  sort!: string;
}
