export interface Tenant {
  partitionKey: string;
  sortedKey: string;
  name: string;
  route: string;
  created: string;
  modified: string;
}
