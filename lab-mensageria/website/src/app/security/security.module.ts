import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { SecurityRoutingModule } from './security-routing.module';
import { SecurityHomeComponent } from './views/security-home/security-home.component';
import { TenantCreateComponent } from './views/tenant-create/tenant-create.component';
import { TenantEditComponent } from './views/tenant-edit/tenant-edit.component';
import { TenantHomeComponent } from './views/tenant-home/tenant-home.component';
import { TenantInfoComponent } from './views/tenant-info/tenant-info.component';
import { CardMenuCreateComponent } from './views/card-menu-create/card-menu-create.component';
import { CardMenuHomeComponent } from './views/card-menu-home/card-menu-home.component';
import { CardMenuAppCreateComponent } from './views/card-menu-app-create/card-menu-app-create.component';
import { CardMenuPreviewComponent } from './views/card-menu-preview/card-menu-preview.component';


@NgModule({
  declarations: [
    SecurityHomeComponent,
    TenantHomeComponent,
    TenantInfoComponent,
    TenantCreateComponent,
    TenantEditComponent,
    CardMenuCreateComponent,
    CardMenuHomeComponent,
    CardMenuAppCreateComponent,
    CardMenuPreviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SecurityRoutingModule,
    SharedModule
  ]
})
export class SecurityModule { }
