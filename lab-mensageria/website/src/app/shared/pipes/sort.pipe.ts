import { Injectable, Pipe, PipeTransform } from '@angular/core';

export type SortOrder = 'asc' | 'desc';

@Injectable()
@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {
  transform(value: any[], sortOrder: string): any {
    if (sortOrder === 'asc') {
      return value?.sort((a:any, b:any) => (a?.sort > b?.sort) ? 1 : -1);
    }
    else {
      console.log('sorting desc');
      return value?.sort((a:any, b:any) => (a?.sort > b?.sort) ? -1 : 1);
    }

  }
}

