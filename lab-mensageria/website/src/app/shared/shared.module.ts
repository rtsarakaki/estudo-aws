import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlertDismissibleComponent } from './controls/alert/alert-dismissible/alert-dismissible.component';
import { AlertModalComponent } from './controls/alert/alert-modal/alert-modal.component';
import { ConfirmModalComponent } from './controls/alert/confirm-modal/confirm-modal.component';
import { InfoToolbarComponent } from './controls/data-controls/info-toolbar/info-toolbar.component';
import { SearchByNameComponent } from './controls/data-controls/search-by-name/search-by-name.component';
import { SearchTitleComponent } from './controls/data-controls/search-title/search-title.component';
import { FormDebugComponent } from './controls/form-debug/form-debug.component';
import { InputFieldComponent } from './controls/input-field/input-field.component';
import { TextAreaFieldComponent } from './controls/textarea-field/textarea-field.component';
import { SortPipe } from './pipes/sort.pipe';

@NgModule({
  declarations: [
    AlertModalComponent,
    ConfirmModalComponent,
    InputFieldComponent,
    TextAreaFieldComponent,
    FormDebugComponent,
    AlertDismissibleComponent,
    SortPipe,
    SearchByNameComponent,
    SearchTitleComponent,
    InfoToolbarComponent,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    AlertModalComponent,
    InputFieldComponent,
    FormDebugComponent,
    TextAreaFieldComponent,
    AlertDismissibleComponent,
    SortPipe,
    SearchByNameComponent,
    SearchTitleComponent,
    InfoToolbarComponent
  ],
  entryComponents: [AlertModalComponent],
})
export class SharedModule {}
