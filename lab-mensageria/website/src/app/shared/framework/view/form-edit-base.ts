import { Component, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';
import { CrudServiceBase } from 'src/app/shared/framework/service/crud-service-base';

@Component({
  template: '',
})
export class FormEditBase<T> {
  deleted = false;
  inscricao!: Subscription;
  item!: T;
  submitted = false;
  service!: CrudServiceBase<T>;
  form!: FormGroup;

  @ViewChild('alert') alert!: AlertDismissibleComponent;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.inscricao = this.activatedRoute.data.subscribe((info) => {
      if (info.dados) {
        this.deleted = false;
        this.item = info.dados.data;
      } else {
        console.log('sem dados');
      }
    });
  }

  ngOnDestroy() {
    if (this.inscricao !== undefined) {
      this.inscricao.unsubscribe();
    }
  }

  onCreate() {
    this.submitted = true;
    if (this.form.valid) {
      this.service.create(this.form.value).subscribe(
        (success:any) => {
          this.submitted = false;
          this.item = <T>success.data;
          this.alert.show(
            'Sucesso!',
            'Registro gravado com sucesso!',
            AlertTypes.SUCCESS
          );
        },
        (error) => {
          this.alert.show(
            'Oops, algo deu errado!',
            error.message,
            AlertTypes.DANGER
          );
        }
      );
    }
  }

  onEdit() {
    this.submitted = true;
    if (this.form.valid) {
      this.service.update(this.item).subscribe(
        (success: any) => {
          console.log(success);
          if (success.IsSuccess) {
            this.submitted = false;
            this.alert.show(
              'Sucesso!',
              'Registro gravado com sucesso!',
              AlertTypes.SUCCESS
            );
          } else {
            this.alert.show(
              'Oops, algo deu errado!',
              success.message,
              AlertTypes.DANGER
            );
          }
        },
        (error: any) => {
          this.alert.show(
            'Oops, algo deu errado!',
            error.message,
            AlertTypes.DANGER
          );
        }
      );
    }
  }
}
