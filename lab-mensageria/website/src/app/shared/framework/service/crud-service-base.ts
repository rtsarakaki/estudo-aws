import { HttpClient } from '@angular/common/http';
import { first, take } from 'rxjs/operators';

import { CrudModelBase } from './crud-model-base';

export class CrudServiceBase<T> {

  constructor(protected http: HttpClient, private API_URL: string) { }

  list() {
    return this.http.get<T[]>(this.API_URL)
      .pipe(
        first()
      )
  }

  getById(sortedKey: string) {
    return this.http.get<T>(`${this.API_URL}/${sortedKey}`).pipe(take(1));
  }

  getByContext(value: string) {
    return this.http.get(`${this.API_URL}/bycontext/${value}`).pipe(first());
  }

  getByName(value: string) {
    return this.http.get(`${this.API_URL}/byname/${value}`).pipe(first());
  }

  create(data: T) {
    return this.http.post(this.API_URL, data).pipe(take(1));
  }

  update(data: T) {
    return this.http.patch(this.API_URL, data).pipe(take(1));
  }

  delete(sortedKey: string){
    return this.http.delete(`${this.API_URL}/${sortedKey}`).pipe(take(1));
  }

  save(data: T) {
    if ((<CrudModelBase>(<unknown>data)).sortedKey) {
      return this.update(data);
    }
    else {
      return this.create(data);
    }
  }
}
