import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'ctrl-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  @ViewChild('deleteModal') deleteModal:any;

  @Input() title: string = 'Confirmação';
  @Input() message: string = 'Deseja continuar?';
  @Input() confirmButtonText: string = 'Ok';
  @Input() cancelButtonText: string = 'Cancelar';

  confirmResult!: Subject<boolean>;

  constructor(public modalRef?: BsModalRef) { }

  ngOnInit(): void {
    this.confirmResult = new Subject();
  }

  onConfirm() {
    this.result(true);
  }

  onCancel(): void {
    this.result(false);
  }

  result(confirm: boolean) {
    this.confirmResult.next(confirm);
    this.modalRef?.hide();
  }

}
