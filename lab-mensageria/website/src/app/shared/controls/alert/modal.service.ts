import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertModalComponent } from './alert-modal/alert-modal.component';

export enum AlertTypes {
  DANGER = 'danger',
  INFO = 'info',
  SUCCESS = 'success'
}

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalRef?: BsModalRef;

  constructor(private modalService: BsModalService) { }

  showAlert(message: string, type: AlertTypes, dismissTimeout?: number) {
    this.modalRef = this.modalService.show(AlertModalComponent);
    const alertModal = <AlertModalComponent>this.modalRef.content;

    alertModal.type = type;
    alertModal.message = message;

    if (dismissTimeout) {
      setTimeout(() => {
        this.modalService.hide();
      }, dismissTimeout);
    }
  }

  showConfirm(title: string, message: string, confirmButtonText?: string, cancelButtonText?: string) {

    this.modalRef = this.modalService.show(ConfirmModalComponent);

    const confirmModal = <ConfirmModalComponent>this.modalRef.content;
    confirmModal.title = title;
    confirmModal.message = message;
    confirmModal.confirmButtonText = confirmButtonText ? confirmButtonText : confirmModal.confirmButtonText;
    confirmModal.cancelButtonText = cancelButtonText ? cancelButtonText : confirmModal.cancelButtonText;
    return confirmModal.confirmResult;
  }
}
