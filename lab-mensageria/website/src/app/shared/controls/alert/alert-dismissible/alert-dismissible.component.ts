import { Component, OnInit, Input } from '@angular/core';
import { AlertTypes } from '../modal.service';

@Component({
  selector: 'ctrl-alert-dismissible',
  templateUrl: './alert-dismissible.component.html',
  styleUrls: ['./alert-dismissible.component.scss']
})
export class AlertDismissibleComponent implements OnInit {

  @Input() title = 'title';
  @Input() message = 'message';
  @Input() type = 'danger';
  get class() {
    return `alert alert-${this.type} alert-dismissible fade show`;
  }
  visible = false;

  constructor() { }

  ngOnInit(): void {
  }

  onClose() {
    this.visible = false;
  }

  show(temp: string, message: string, type: AlertTypes) {
    let title = '';
    switch (type) {
      case  AlertTypes.DANGER:
        title = 'Oops, algo deu errado!';
        break;
      case  AlertTypes.INFO:
        title = 'Informação!';
        break;
      case  AlertTypes.SUCCESS:
        title = 'Sucesso!';
        break;
      default:
        title = 'Oops, algo deu errado!';
    }

    this.type = type;
    this.title = title;
    this.message = message;
    this.visible = true;
  }

  icon(type: string) {
    return this.type === type;
  }
}
