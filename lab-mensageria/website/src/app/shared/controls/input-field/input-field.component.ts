import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FieldBaseControl } from '../field-base-control';

const INPUT_FIELD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputFieldComponent),
  multi: true
};

@Component({
  selector: 'ctrl-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
  providers: [INPUT_FIELD_VALUE_ACCESSOR]
})
export class InputFieldComponent extends FieldBaseControl  implements ControlValueAccessor {
  @Input() type = 'text';
  @Input() id!: string;
  @Input() label: string = 'label';
  @Input() placeholder = '';
  @Input() isReadOnly = false;
  @Input() classeCss = '';
  @Input() messagePaternError = 'O valor do campo não é válido.';

  @Input() form!: FormGroup;
  @Input() control!: any;

  @Input() submitted = false;

  @Input() ignoreOkMessage = true;
}
