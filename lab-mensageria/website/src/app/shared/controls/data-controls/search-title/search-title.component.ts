import { SearchByNameComponent } from './../search-by-name/search-by-name.component';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ctrl-search-title',
  templateUrl: './search-title.component.html',
  styleUrls: ['./search-title.component.scss']
})
export class SearchTitleComponent implements OnInit {
  @Input() title!: string;
  @Input() buttonTitle!: string;
  @Input() searchControl!: SearchByNameComponent;
  @Output() onCreate = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    this.onCreate.emit();
  }

}
