import { FormEditBase } from './../../../framework/view/form-edit-base';
import { AlertTypes } from 'src/app/shared/controls/alert/modal.service';
import { take, switchMap } from 'rxjs/operators';
import { ModalService } from './../../alert/modal.service';
import { AlertDismissibleComponent } from 'src/app/shared/controls/alert/alert-dismissible/alert-dismissible.component';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { CrudServiceBase } from 'src/app/shared/framework/service/crud-service-base';

@Component({
  selector: 'ctrl-info-toolbar',
  templateUrl: './info-toolbar.component.html',
  styleUrls: ['./info-toolbar.component.scss']
})
export class InfoToolbarComponent implements OnInit {
  @Input() formEdit: any;
  @Input() item!: any;
  @Input() alert!: AlertDismissibleComponent;
  @Input() service!: CrudServiceBase<any>;
  @Input() title!: string;
  @Input() modalSuccessTitle = 'Sucesso! :-)';
  @Input() modalSuccessMessage = 'Registro apagado com sucesso.';
  @Input() modalErrorTitle = 'Oops, algo deu errado. :-/';
  @Input() modalTitle = 'Apagar Registro';
  @Input() modalTitleMessage = 'Por favor, confirme se realmente deseja apagar o registro';
  @Output() onEditItem = new EventEmitter<any>();

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): void {

  }

  onDelete() {
    const result$ = this.modalService.showConfirm(this.modalTitle, this.modalTitleMessage + ' (' + this.item.sortedKey + ' - ' + this.item.name + ')', 'Confirmar', 'Desistir');
    result$.asObservable()
      .pipe(
        take(1),
        switchMap(result => result ? this.service.delete(this.item.sortedKey) : EMPTY)
      )
      .subscribe(
        success => {
          this.formEdit.deleted = true;
          this.alert.show(this.modalSuccessTitle, this.modalSuccessMessage, AlertTypes.SUCCESS);
        },
        error => {
          this.alert.show(this.modalErrorTitle, error, AlertTypes.DANGER)
        },
        () => console.log('request completo')
      );
  }

  onEdit() {
    this.onEditItem.emit();
  }

}
