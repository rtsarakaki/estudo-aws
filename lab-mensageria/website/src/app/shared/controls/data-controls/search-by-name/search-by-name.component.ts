import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { CrudServiceBase } from './../../../framework/service/crud-service-base';

@Component({
  selector: 'ctrl-search-by-name',
  templateUrl: './search-by-name.component.html',
  styleUrls: ['./search-by-name.component.scss']
})
export class SearchByNameComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  items: any;
  @Input() service!: CrudServiceBase<any>;
  @Output() onSelectItem = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: [null]
    })

    this.loadItems();
  }

  loadItems() {
    if (this.form.get('search')?.value) {
      this.onSearch(this.form.get('search')?.value);
    }
    else {
      this.listTenants();
    }
  }

  onSearch(value: string) {
    this.items = null;
    this.service.getByName(value).subscribe(
      (success: any) => {
        this.submitted = false;
        this.items = success.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  listTenants() {
    this.service.list().subscribe(
      (success: any) => {
        this.submitted = false;
        this.items = success.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onSelect(item: any) {
    this.onSelectItem.emit(item);
  }
}
