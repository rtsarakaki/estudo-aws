import { forwardRef } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

const INPUT_FIELD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FieldBaseControl),
  multi: true
};

export class FieldBaseControl  implements ControlValueAccessor {
  type = 'text';
  id!: string;
  label: string = 'label';
  placeholder = 'placeholder';
  isReadOnly = false;
  classeCss = '';
  messagePaternError = 'O valor do campo não é válido.';
  ignoreOkMessage = true;

  form!: FormGroup;
  control!: any;

  submitted = false;

  private innerValue: any;

  get value() {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCb(v);
    }
  }

  onChangeCb: (_: any) => void = () => {};
  onTouchedCb: (_: any) => void = () => {};

  writeValue(v: any): void {
    this.value = v;
  }

  registerOnChange(fn: any): void {
    this.onChangeCb = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCb = fn;
  }

  hasTouched(field: string) {
    return (this.control?.touched || this.control?.dirty || this.submitted);
  }

  hasError(field: string) {
    return this.control?.errors;
  }

  getErrorMsg(field: string) {
    const errors = this.hasError(field);
    if (errors) {
      if (errors['required']) {
        return `Por favor, preencha o campo ${field}.`;
      }
      else if (errors['email']) {
        return `Por favor, confira se o e-mail está digitado corretamente.`;
      }
      else if (errors['minlength']) {
        return `O campo ${field} deve ter pelo menos ${errors['minlength'].requiredLength} caracteres.`;
      }
      else if (errors['maxlength']) {
        return `O campo ${field} deve no máximo ${errors['maxlength'].requiredLength} caracteres.`;
      }
      else if (errors['pattern']) {
        return this.messagePaternError;
      }
      else {
        return JSON.stringify(errors);
      }
    }
    return 'Tudo ok!';
  }

}
