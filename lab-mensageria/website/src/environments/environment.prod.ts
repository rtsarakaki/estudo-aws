export const environment = {
  production: true,
  API: 'http://localhost:7000/',
  AWS_REGION: 'us-east-1',
  AWS_IDENTITY_POOL_ID: 'us-east-1:1df01fce-aad5-4cac-9bfb-0ed79367e62f',
  AWS_COGNITO_USER_POOL_ID: 'us-east-1_3O5goHDLe',
  AWS_COGNITO_CLIENT_ID: '3rlo3k552vi78bgfibd9q343mk',
  AWS_COGNITO_IDP_ENDPOINT: '',
  AWS_COGNITO_IDENTITY_ENDPOINT: '',
  AWS_STS_ENDPOINT: null,
  // ddbTableName: '',
  // dynamodb_endpoint: '',
  // bucketRegion: '',
  // rekognitionBucket: '',
  // s3_endpoint: '',
  // albumName: '',
  /*
    (?=.*\d)              // deve conter ao menos um dígito
    (?=.*[a-z])           // deve conter ao menos uma letra minúscula
    (?=.*[A-Z])           // deve conter ao menos uma letra maiúscula
    (?=.*[$*&@#])         // deve conter ao menos um caractere especial
    (?=.*[$@$!%*?&])      // deve conter ao menos 8 dos caracteres mencionados
   */
  REGEX_PASSWORD: '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}',
  ROUTE_INITIAL: '',
};
