// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API: 'http://localhost:7000/',
  AWS_REGION: 'us-east-1',
  AWS_IDENTITY_POOL_ID: 'us-east-1:fd24ed75-4b90-4d25-95a4-1a780ec9845a',
  AWS_COGNITO_USER_POOL_ID: 'us-east-1_yTPARRaR1',
  AWS_COGNITO_CLIENT_ID: '7c64i57d0aedpkeebh37qqck6r',
  AWS_COGNITO_IDP_ENDPOINT: '',
  AWS_COGNITO_IDENTITY_ENDPOINT: '',
  AWS_STS_ENDPOINT: '',
  // ddbTableName: '',
  // dynamodb_endpoint: '',
  // bucketRegion: '',
  // rekognitionBucket: '',
  // s3_endpoint: '',
  // albumName: '',
  /*
    (?=.*\d)              // deve conter ao menos um dígito
    (?=.*[a-z])           // deve conter ao menos uma letra minúscula
    (?=.*[A-Z])           // deve conter ao menos uma letra maiúscula
    (?=.*[$*&@#])         // deve conter ao menos um caractere especial
    (?=.*[$@$!%*?&])      // deve conter ao menos 8 dos caracteres mencionados
   */
  REGEX_PASSWORD: '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}',
  ROUTE_INITIAL: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
