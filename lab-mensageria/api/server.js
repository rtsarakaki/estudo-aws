
/**
 * Module dependencies.
 */
const http = require('http');
const debug = require('debug')('node-rest:server');
const app = require('./app');

/**
 * Get port from environment and store in Express.
 */
 const port = normalizePort(process.env.port || '3000');

/**
 * Create HTTP server na porta cofigurada
 */
const server = http.createServer(app); //criar o servidor

/**
 * Listen on priovided port, on all network interface
 */
 server.listen(port, function () {
    console.log('Server listening on port: ', port);
});

server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listening for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe' + port : 'Port' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);
}