#!/usr/bin/env node

/**
 * Module dependencies.
 */
const express = require('express');
const debug = require('debug')('node-rest:server');
const compression = require('compression');
const morgan = require('morgan');
const bodyParser = require('body-parser')
const cors = require('cors');

/**
 * Cria aplicação
 */
const app = express();

/**
 * Configura a aplicação
 */
app.use(cors());
app.use(compression());
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * Remove cors
 */
const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
};

app.use(cors(corsOptions));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Header', 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).send({});
    }

    next();
});

const HelloWorldRoute = require('./modules/hello-world/routes/hello-world.route');
const AwsSqsRoute = require('./modules/aws-sqs/routes/aws-sqs.route');
const Security = require('./modules/security/routes/security.route');

app.use('/hello-world', HelloWorldRoute);
app.use('/aws-sqs', AwsSqsRoute);
app.use('/security', Security);

//QUANDO NÃO ENCONTRA NENHUMA ROTA
app.use((req, res, next) => {
    const erro = new Error('Não encontrado!');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    return res.send({
        erro: {
            mensagem: error.message
        }
    });
});

module.exports = app;