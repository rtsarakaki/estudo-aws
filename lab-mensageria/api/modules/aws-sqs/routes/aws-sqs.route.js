const express = require('express');
const router = express.Router();
const awsSqsController = require('../controllers/aws-sqs.controller')

router.post('/receive-message', awsSqsController.receiveMessage);
router.post('/send-message', awsSqsController.sendMessage);
router.post('/delete-message', awsSqsController.deleteMessage);
router.post('/change-message-visibility', awsSqsController.changeMessageVisibility);

module.exports = router;