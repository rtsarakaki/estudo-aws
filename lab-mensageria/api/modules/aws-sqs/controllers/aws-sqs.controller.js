const AWS = require('aws-sdk')
AWS.config.update({ region: 'us-east-1' });

const config = {
  endpoint: new AWS.Endpoint('https://sqs.us-east-1.amazonaws.com/417796969030/'),
  accessKeyId: 'AKIAWCRVD2JDH46MBYUB',
  secretAccessKey: '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',
  region: 'us-east-1'
}

const QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/417796969030/lab';

const sqs = new AWS.SQS(config);

exports.receiveMessage = async (req, res, next) => {

  try {
    const params = req.body;
    console.log(params);

    sqs.receiveMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Sucesso.',
          result: data
        }

        return res.status(200).send(response);

      }
    });

  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.sendMessage = async (req, res, next) => {

  try {
    const params = req.body;
    console.log(params);

    sqs.sendMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Mensagem criada com sucesso.',
          result: data
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.deleteMessage = async (req, res, next) => {
  try {

    const params = req.body;

    // console.log(req.body);
    console.log(params);

    sqs.deleteMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'Mensagem apagada com sucesso.'
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.changeMessageVisibility = async (req, res, next) => {
  try {

    const params = req.body;

    // console.log(req.body);
    console.log(params);

    sqs.changeMessageVisibility(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      }
      else {

        const response = {
          message: 'VisibilityTimeout alterado com sucesso.'
        }

        return res.status(200).send(response);

      }
    });
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};