const express = require('express');
const router = express.Router();
const helloWorldController = require('../controllers/hello-world.controller')

router.get('/', helloWorldController.helloWorld);

module.exports = router;