const express = require('express');
const router = express.Router();

const apiTenant = require('../controllers/tenant.controller')
router.get('/tenant', apiTenant.list);
router.get('/tenant/:sortedKey', apiTenant.getById);
router.post('/tenant', apiTenant.create);
router.patch('/tenant', apiTenant.update);
router.delete('/tenant/:sortedKey', apiTenant.delete);

const apiMenu = require('../controllers/menu.controller')
router.get('/menu', apiMenu.list);
router.get('/menu/:sortedKey', apiMenu.getById);
router.post('/menu', apiMenu.create);
router.patch('/menu', apiMenu.update);
router.delete('/menu/:sortedKey', apiMenu.delete);
router.get('/menu/bycontext/:context', apiMenu.getByContext);
router.get('/menu/byname/:name', apiMenu.getByName);

const apiCardMenu = require('../controllers/card-menu.controller')
router.get('/card-menu', apiCardMenu.list);
router.get('/card-menu/:sortedKey', apiCardMenu.getById);
router.post('/card-menu', apiCardMenu.create);
router.patch('/card-menu', apiCardMenu.update);
router.delete('/card-menu/:sortedKey', apiCardMenu.delete);
router.get('/card-menu/bycontext/:context', apiCardMenu.getByContext);
router.get('/card-menu/byname/:name', apiCardMenu.getByName);

module.exports = router;