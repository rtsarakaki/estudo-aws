const express = require('express');
const moment = require('moment');
const _ = require('underscore');
const { uuid } = require('uuidv4');

const AWS = require('aws-sdk')
AWS.config.update({ region: 'us-east-1' });

const config = {
  accessKeyId: 'AKIAWCRVD2JDH46MBYUB',
  secretAccessKey: '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',
  region: 'us-east-1'
}

const docClient = new AWS.DynamoDB.DocumentClient(config);

const TABLE_NAME = 'db-security';
const PREFIX = 'card-menu';

exports.create = async (req, res, next) => {
  const date = new Date();
  let unique_id = moment().unix();// ou uuid()
  let item = req.body;
  
  console.log(req);

  item.partitionKey = PREFIX;
  item.sortedKey = PREFIX + '|' + unique_id;
  item.searchName = cleanText(item.name);
  item.searchContext = cleanText(item.context);
  item.created = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
  item.sort = item.order + item.name;

  console.log(item);

  docClient.put({
    TableName: TABLE_NAME,
    Item: item
  }, (err, data) => {
    if (err) {
      console.log(err);
      return res.status(err.statusCode).send({
        message: err.message,
        status: err.statusCode
      });
    } else {
      return res.status(200).send(item);
    }
  });
};

exports.update = async (req, res, next) => {
  const date = new Date();
  let item = req.body;
  item.modified = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
  item.searchName = cleanText(item.name);
  item.searchContext = cleanText(item.context);
  item.sort = item.order + item.name;

  console.log(item);

  docClient.put({
    TableName: TABLE_NAME,
    Item: item,
    ConditionExpression: '#sk = :sk',
    ExpressionAttributeNames: {
      '#sk': 'sortedKey'
    },
    ExpressionAttributeValues: {
      ':sk': item.sortedKey
    }
  }, (err, data) => {
    if (err) {
      console.log(err);
      return res.status(err.statusCode).send({
        message: err.message,
        status: err.statusCode
      });
    } else {
      return res.status(200).send(item);
    }
  });
};

exports.list = async (req, res, next) => {
  let limit = req.query.limit ? parseInt(req.query.limit) : 20;
  let params = {
    TableName: TABLE_NAME,
    KeyConditionExpression: "partitionKey = :partitionKey",
    ExpressionAttributeValues: {
        ":partitionKey": PREFIX
    },
    Limit: limit,
    ScanIndexForward: false
  };

  if (req.query.start) {
    params.ExclusiveStartKey = {
      partitionKey: PREFIX,
      sortedKey: req.query.start
    }
  }

  docClient.query(params, (err, data) => {
    if (err) {
      console.log(err);
      return res.status(err.statusCode).send({
        message: err.message,
        status: err.statusCode
      });
    } else {
      return res.status(200).send(data);
    }
  });
};

exports.getById = async (req, res, next) => {
  let sortedKey = req.params.sortedKey;

  let params = {
      TableName: TABLE_NAME,
      KeyConditionExpression: "partitionKey = :partitionKey and sortedKey = :sortedKey",
      ExpressionAttributeValues: {
          ":partitionKey": PREFIX,
          ":sortedKey": sortedKey
      },
      Limit: 1
  };

  docClient.query(params, (err, data) => {
      if (err) {
          console.log(err);
          return res.status(err.statusCode).send({
              message: err.message,
              status: err.statusCode
          });
      } else {
          if (!_.isEmpty(data.Items)) {
              return res.status(200).send(data.Items[0]);
          } else {
              return res.status(404).send();
          }
      }
  });
};

exports.delete = async (req, res, next) => {
  let params = {
      TableName: TABLE_NAME,
      Key: {
          partitionKey: PREFIX,
          sortedKey: req.params.sortedKey
      }
  };

  docClient.delete(params, (err, data) => {
      if (err) {
          console.log(err);
          return res.status(err.statusCode).send({
              message: err.message,
              status: err.statusCode
          });
      } else {
          return res.status(200).send(data);
      }
  });
};

exports.getByContext = async (req, res, next) => {
  let limit = req.query.limit ? parseInt(req.query.limit) : 200;
  let context = cleanText(req.params.context);

  let params = {
      TableName: TABLE_NAME,
      IndexName: "context-lsi",
      KeyConditionExpression: "#partitionKey = :partitionKey and #context = :context",
      ExpressionAttributeNames: {
        "#partitionKey": "partitionKey",
        "#context": "searchContext"
      },
      ExpressionAttributeValues: {
          ":partitionKey": PREFIX,
          ":context": context
      },
      Limit: limit
  };

  docClient.query(params, (err, data) => {
      if (err) {
          console.log(err);
          return res.status(err.statusCode).send({
              message: err.message,
              status: err.statusCode
          });
      } else {
          if (!_.isEmpty(data.Items)) {
              return res.status(200).send(data);
          } else {
              return res.status(404).send();
          }
      }
  });
};

exports.getByName = async (req, res, next) => {
  let limit = req.query.limit ? parseInt(req.query.limit) : 200;
  let name = cleanText(req.params.name);

  let params = {
      TableName: TABLE_NAME,
      IndexName: "name-lsi",
      KeyConditionExpression: "#partitionKey = :partitionKey and begins_with(#name, :name)",
      ExpressionAttributeNames: {
        "#partitionKey": "partitionKey",
        "#name": "searchName"
      },
      ExpressionAttributeValues: {
          ":partitionKey": PREFIX,
          ":name": name
      },
      Limit: limit
  };

  docClient.query(params, (err, data) => {
      if (err) {
          console.log(err);
          return res.status(err.statusCode).send({
              message: err.message,
              status: err.statusCode
          });
      } else {
          if (!_.isEmpty(data.Items)) {
              return res.status(200).send(data);
          } else {
              return res.status(404).send();
          }
      }
  });
};

function cleanText (text)
{
  if (text) {
    text = text.toLowerCase();                                                         
    text = text.replace(new RegExp('[ÁÀÂÃ]','gi'), 'a');
    text = text.replace(new RegExp('[ÉÈÊ]','gi'), 'e');
    text = text.replace(new RegExp('[ÍÌÎ]','gi'), 'i');
    text = text.replace(new RegExp('[ÓÒÔÕ]','gi'), 'o');
    text = text.replace(new RegExp('[ÚÙÛ]','gi'), 'u');
    text = text.replace(new RegExp('[Ç]','gi'), 'c');
    return text;
  }
  else {
    return '';
  }                
}