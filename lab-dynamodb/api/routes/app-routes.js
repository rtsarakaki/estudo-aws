const express = require('express');
const moment = require('moment');
const _ = require('underscore');
const { uuid } = require('uuidv4');

const AWS = require('aws-sdk')
AWS.config.update({ region: 'us-east-1' });

const config = {
    accessKeyId: 'AKIAWCRVD2JDH46MBYUB',
    secretAccessKey: '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',
    region: 'us-east-1'
}

const docClient = new AWS.DynamoDB.DocumentClient(config);

const router = express.Router();

const tableName = 'td_notes';
const user_id = '1';
const user_name = 'rtsarakaki'

router.post('/api/note', (req, res, next) => {

    let item = req.body.Item;
    item.user_id = user_id;
    item.user_name = user_name;
    item.note_id = user_id + ':' + uuid();
    item.timestamp = moment().unix();
    item.expires = moment().add(90, 'days').unix();

    docClient.put({
        TableName: tableName,
        Item: item
    }, (err, data) => {
        if (err) {
            console.log(err);
            return res.status(err.statusCode).send({
                message: err.message,
                status: err.statusCode
            });
        } else {
            return res.status(200).send(item);
        }
    });
});

router.patch('/api/note', (req, res, next) => {

    let item = req.body.Item;
    item.user_id = user_id;
    item.user_name = user_name;
    item.expires = moment().add(90, 'days').unix();

    docClient.put({
        TableName: tableName,
        Item: item,
        ConditionExpression: '#t = :t',
        ExpressionAttributeNames: {
            '#t': 'timestamp'
        },
        ExpressionAttributeValues: {
            ':t': item.timestamp
        }
    }, (err, data) => {
        if (err) {
            console.log(err);
            return res.status(err.statusCode).send({
                message: err.message,
                status: err.statusCode
            });
        } else {
            return res.status(200).send(item);
        }
    });
});

router.get('/api/note', (req, res, next) => {
    let limit = req.query.limit ? parseInt(req.query.limit) : 5;
    let params = {
        TableName: tableName,
        KeyConditionExpression: "user_id = :uid",
        ExpressionAttributeValues: {
            ":uid": user_id
        },
        Limit: limit,
        ScanIndexForward: false
    };

    let startTimestamp = req.query.start ? parseInt(req.query.start) : 0;

    if (startTimestamp > 0) {
        params.ExclusiveStartKey = {
            user_id: user_id,
            timestamp: startTimestamp
        }
    }

    docClient.query(params, (err, data) => {
        if (err) {
            console.log(err);
            return res.status(err.statusCode).send({
                message: err.message,
                status: err.statusCode
            });
        } else {
            return res.status(200).send(data);
        }
    });
});

router.get('/api/note/:note_id', (req, res, next) => {
    let note_id = req.params.note_id;

    let params = {
        TableName: tableName,
        IndexName: "note_id-index",
        KeyConditionExpression: "note_id = :note_id",
        ExpressionAttributeValues: {
            ":note_id": note_id
        },
        Limit: 1
    };

    docClient.query(params, (err, data) => {
        if (err) {
            console.log(err);
            return res.status(err.statusCode).send({
                message: err.message,
                status: err.statusCode
            });
        } else {
            if (!_.isEmpty(data.Items)) {
                return res.status(200).send(data.Items[0]);
            } else {
                return res.status(404).send();
            }
        }
    });
});

router.delete('/api/note/:timestamp', (req, res, next) => {

    let timestamp = parseInt(req.params.timestamp);
    let params = {
        TableName: tableName,
        Key: {
            user_id: user_id,
            timestamp: timestamp
        }
    };

    docClient.delete(params, (err, data) => {
        if (err) {
            console.log(err);
            return res.status(err.statusCode).send({
                message: err.message,
                status: err.statusCode
            });
        } else {
            return res.status(200).send();
        }
    });
});

module.exports = router;