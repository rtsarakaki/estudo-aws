#!/usr/bin/env node

/**
 * Module dependencies.
 */
const http = require('http');
const debug = require('debug')('node-rest:server');
const compression = require('compression');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');

//const app = require('./app');

/**
 * Cria express
 */
const app = express();

/**
 * Configura a aplicação
 */
app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, './public')));

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || '3000');

const appRoutes = require('./routes/app-routes');
app.use(appRoutes);

// View engine setup
app.set('views', path.join(__dirname, './app/views'));
app.set('view engine', 'ejs');
 
// With middleware
app.use('/', function(req, res, next){
    res.render('home', {layout:false})
    next();
});
 
app.get('/', function(req, res){
    console.log("Render Working")
    res.send();
});

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on priovided port, on all network interface
 */
server.listen(port, function () {
    console.log('Server listening on port: ', port);
});

server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listening for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe' + port : 'Port' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);
}